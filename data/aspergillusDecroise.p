% aspergillus

:-
cinnamomumZeylanicum : [ lauraceae ], 
chromolaenaOdorata & aspiliaAfricana & ageratumConyzoides : [ asteraceae ],
cinnamomumZeylanicum & chromolaenaOdorata & aspiliaAfricana & ageratumConyzoides : [ aromatic ],
cinnamomumZeylanicum : [ comestible ],
chromolaenaOdorata & aspiliaAfricana & ageratumConyzoides : [ toxic ],
cinnamomumZeylanicum & chromolaenaOdorata & aspiliaAfricana & ageratumConyzoides : [ evergreen ],
aspiliaAfricana : [ contraceptive ],
ageratumConyzoides : [ antidysenteric ],
cinnamomumZeylanicum : [ applicOil ],
chromolaenaOdorata & aspiliaAfricana & ageratumConyzoides : [ applicEssentialOil ],
chromolaenaOdorata & aspiliaAfricana & ageratumConyzoides : [ applicExtract ],
%
aspergillusParasiticus & aspergillusOchraceus : [ attacksPeanuts ],
aspergillusFlavus & aspergillusTamarii : [ attacksCheese ],
aspergillusTamarii : [ attacksRice ],
aspergillusFlavus & aspergillusParasiticus & aspergillusOchraceus & aspergillusTamarii : [ aspergillus ],
%
Namibia & SouthernAfrica : [ australAfrica ],
Nigeria & Benin : [ westernAfrica ],
%
cinnamomumZeylanicum : [ treat aspergillusFlavus & aspergillusTamarii ],
aspiliaAfricana : [ treat  aspergillusParasiticus ],
chromolaenaOdorata : [ treat  aspergillusOchraceus ],
ageratumConyzoides : [ treat aspergillusFlavus ],
%cinnamomumZeylanicum : [ treat  aspergillusTamarii ],
%
aspergillusFlavus : [ isFoundIn Namibia ],
aspergillusTamarii : [ isFoundIn  SouthernAfrica ],
aspergillusOchraceus : [ isFoundIn  Nigeria ],
aspergillusParasiticus : [ isFoundIn  Benin ],
%
Benin : [ possesses aspiliaAfricana ],
Nigeria : [ possesses  chromolaenaOdorata ].

