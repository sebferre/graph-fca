## Web application ##

A Web application now exists, allowing you to simply upload the text file containing the description of your knowledge graph, and to visualize the computed graph concepts in the browser.

Visit [Graph-FCA on Allgo](https://allgo.inria.fr/app/graphfca).

## Compile ##

In the [Downloads section](https://bitbucket.org/sebferre/g-fca/downloads/), there are executables **gfca**, mostly for Linux x64 and MacOS architectures. Download the latest version.

If you want to compile from the source, you will need in addition to the source:

* [OCaml](http://ocaml.org/) compilers
* my repository [ocaml-lib](https://bitbucket.org/sebferre/ocaml-lib/src/master/), available here on Bitbucket

You may have to adapt the makefile to your needs.

## Run ##

Run `gfca -help` to learn about command line options.
The syntax of input files is specified in source file `program.ml`.
Examples are given in the 'data/' subdirectory.

## Documentation ##

The [user manual](https://bitbucket.org/sebferre/graph-fca/downloads/manual.pdf) describes in detail the input and output formats, as well as the configuration options. For definitions and explanations about Graph-FCA, please look at the research papers listed below.

## Comments ##

Note that this program is a research prototype in beta version. Use results with caution.

To get more information and to refer to the theory behind this program, please read/cite the following research papers.

* Sébastien Ferré, Peggy Cellier. Graph-FCA: An extension of formal concept analysis to knowledge graphs, Discrete Applied Mathematics, 2019. [DOI](https://doi.org/10.1016/j.dam.2019.03.003)
* Sébastien Ferré, Peggy Cellier. Graph-FCA in Practice. In Int. Conf. Conceptual Structures (ICCS), LNCS 9717, pages 107-121, 2016. Springer. [PDF](http://link.springer.com/chapter/10.1007%2F978-3-319-40985-6_9)
* Sébastien Ferré. A Proposal for Extending Formal Concept Analysis to Knowledge Graphs. In J. Baixeries, C. Sacarea, and M. Ojeda-Aciego, editors, Int. Conf. Formal Concept Analysis (ICFCA), LNCS 9113, pages 271-286, 2015. Springer. [PDF](https://hal.inria.fr/hal-01196287/document)