
open Patterns
  
(* generation of concepts *)

exception CtrlC
let int2exn f =
  let behav = Sys.signal Sys.sigint (Sys.Signal_handle (fun _ -> raise CtrlC)) in
  try f ()
  with CtrlC ->
    Sys.set_signal Sys.sigint behav;
    raise CtrlC
	  
module Patternset = Set.Make (struct type t = pattern_repr let compare = Stdlib.compare end)
module Projset = Set.Make (struct type t = int * var list let compare = Stdlib.compare end)

type core_id = int
type var_set = var LSet.t
type inverted_extent = (var * Intset.t) list

type concepts_by_core (* cbc *) =
  { mutable core_id : core_id;
    core_vars : var list;
    pattern : pattern;
    mutable intents : (var * var_set) list;
    mutable inverted_extent : inverted_extent }

let cbcs_nb_extents (cbcs : concepts_by_core list) : int =
  let extents =
    List.fold_left
      (fun res cbc ->
        List.fold_left
          (fun res (x,ext) -> Bintree.add ext res)
          res cbc.inverted_extent)
      Bintree.empty cbcs in
  Bintree.cardinal extents
    
let get_inv_ext ~injective p g
  = Common.prof "Generation.get_inv_ext" (fun () ->
  prerr_string "e"; flush stderr;
  let ext = Patterns.extension p g in
  let ext = if injective then injective_extent ext else ext in
  Patterns.inverted_extent ext)

let get_projections_increasing_card ~only_cores p_inter
  = Common.prof "Generation.get_projections_increasing_card" (fun () ->
  let core, _ = Patterns.core_vars p_inter in
  let core_elements = Intset.elements core in
  let projset = [(Intset.cardinal core, core_elements)] in
  let projset =
    if only_cores
    then projset
    else
      p_inter#var_list
      |> List.fold_left
	   (fun projset v ->
	    if Intset.mem v core
	    then projset
	    else
	      let v_domains = List.map (fun v -> (v, Intset.singleton v)) (v::core_elements) in
	      let v_core, _ = Patterns.core_vars ~init_domains:v_domains p_inter in
	      (Intset.cardinal v_core, Intset.elements v_core)::projset)
	   projset in
  List.sort_uniq Stdlib.compare projset)
    
let init_core ~show_extent ~injective ~min_supp ~max_size g p_inter lset_sx
  = Common.prof "Generation.init_core" (fun () ->
  let join_intents = List.map (fun x -> (x,lset_sx)) lset_sx in
  if show_extent
  then
    let core_proj = Patterns.projection lset_sx p_inter in
    let join_inv_ext = get_inv_ext ~injective core_proj g in
    let good_core =
      List.length lset_sx <= max_size
      && List.for_all (fun (x,ext) -> Intset.cardinal ext >= min_supp) join_inv_ext in
    (good_core, lset_sx, lset_sx, join_intents, join_inv_ext)
  else
    let good_core = List.length lset_sx <= max_size in
    (good_core, lset_sx, lset_sx, join_intents, []))
		
let extend_core
      ~show_extent ~injective ~min_supp ~max_size g proj_patternset p_inter lset_sx
      retracts
      (good_core,core,join_new_sx,join_intents,join_inv_ext as core_data)
  = Common.prof "Generation.extend_core" (fun () ->
  if not good_core (* assuming core is less frequent than the rest *)
     || not (List.length lset_sx <= max_size)
     || not (LSet.contains lset_sx core) (* should not happen *)
  then retracts, core_data
  else
    let delta_sx = LSet.diff lset_sx join_new_sx in
    if LSet.is_empty delta_sx
    then retracts, core_data
    else
      let p_proj = projection lset_sx p_inter in
      let _renaming, repr = p_proj#canonical_repr in
      if Patternset.mem repr !proj_patternset (* pattern already produced *)
      then retracts, (good_core, core, join_new_sx, join_intents, join_inv_ext)
      else begin
	  prerr_string "r"; flush stderr; (* new retract *)
	  proj_patternset := Patternset.add repr !proj_patternset;
	  let join_new_sx, join_intents, join_inv_ext =
	    let delta_intents = List.map (fun x -> (x,lset_sx)) delta_sx in
	    if show_extent
	    then
	      let delta_inv_ext =
		let inv_ext = get_inv_ext ~injective p_proj g in
		List.filter (fun (x,ext) -> List.mem x delta_sx) inv_ext in
	      LSet.union lset_sx join_new_sx, delta_intents @ join_intents, delta_inv_ext @ join_inv_ext
	    else
	      LSet.union lset_sx join_new_sx, delta_intents @ join_intents, join_inv_ext in
	  p_proj::retracts,
          (good_core, core, join_new_sx, join_intents, join_inv_ext)
	end)

let get_core_and_retracts
      ~only_cores ~show_extent ~injective ~min_supp ~max_size
      g proj_patternset p_inter
  = Common.prof "Generation.get_core_and_retracts" (fun () ->
  match get_projections_increasing_card ~only_cores p_inter with
  | [] -> assert false
  | (_,sx)::l ->
     let initial_core =
       init_core ~show_extent ~injective ~min_supp ~max_size g p_inter (LSet.of_list sx) in
     if only_cores
     then ([],initial_core)
     else
       let retracts, extended_core =
	 List.fold_left
	   (fun (retracts, extended_core) (_,sx) ->
             extend_core ~show_extent ~injective ~min_supp ~max_size
               g proj_patternset p_inter (LSet.of_list sx) retracts extended_core)
	   ([],initial_core) l in
       retracts, extended_core)

let join_with_cbc (core, join_pat, join_intents, join_inv_ext) other_cbc
  = Common.prof "Generation.join_with_cbc" (fun () ->
  let subst = other_cbc.pattern#join_with (other_cbc.core_vars,core) join_pat in
  let rename x2 = try List.assoc x2 subst with _ -> assert false in
  let new_intents =
    List.fold_left
      (fun res (x,intent) ->
       if List.mem x core
       then res
       else (rename x, List.map rename intent)::res)
      [] join_intents in
  let new_inv_ext =
    List.fold_left
      (fun res (x,ext) ->
       if List.mem x core
       then res
       else (rename x, ext)::res)
      [] join_inv_ext in
  other_cbc.intents <- new_intents @ other_cbc.intents;
  other_cbc.inverted_extent <- new_inv_ext @ other_cbc.inverted_extent)

  
type concept_key = (core_id * var) list
		
let upper_covers : concepts_by_core list -> concept_key list (* concepts *) * (concept_key * concept_key list) list (* concept upper covers *) =
  let module Concepts =
    Set.Make(struct
	type t = (int * concept_key) * Intset.t (* extension *)
	let compare (k1,_) (k2,_) = Stdlib.compare k1 k2
      end) in
  fun l_cbc ->
  Common.prof "Generation.upper_covers" (fun () ->
  let ht_concepts : (int list, Intset.t * concept_key ref) Hashtbl.t = Hashtbl.create 1003 in
  List.iter
    (fun cbc ->
      let core_id = cbc.core_id in
      List.iter
        (fun (x,ext) ->
          let objs = Intset.elements ext in
          match Hashtbl.find_opt ht_concepts objs with
          | None ->
             Hashtbl.add ht_concepts objs (ext, ref [(core_id,x)])
            | Some (ext, ref_concept) ->
               ref_concept := (core_id,x)::!ref_concept)
        cbc.inverted_extent)
    l_cbc;
  let concepts =
    Hashtbl.fold
      (fun objs (ext, ref_concept) res ->
        !ref_concept :: res)
      ht_concepts [] in
  let all_concepts =
    Hashtbl.fold
      (fun objs (ext, ref_concept) res ->
        Concepts.add ((Intset.cardinal ext, !ref_concept), ext) res)
      ht_concepts Concepts.empty in
  let s_done = ref Concepts.empty in
  let s_todo = ref all_concepts in
  let res = ref [] in
  while not (Concepts.is_empty !s_todo) do
    let ((n,c),ext as c_ext) = Concepts.max_elt !s_todo in
    s_todo := Concepts.remove c_ext !s_todo;
    let upper =
      Concepts.filter
	(fun (_,ext') -> Intset.subset ext ext')
	!s_done in
    let upper_cover =
      let l_cover = ref [] in
      let s_rest = ref upper in
      while not (Concepts.is_empty !s_rest) do
	let ((n',c'),ext' as c_ext') = Concepts.min_elt !s_rest in
	s_rest := Concepts.remove c_ext' !s_rest;
	s_rest := Concepts.filter (fun (c'',ext'') -> not (Intset.subset ext' ext'')) !s_rest;
        assert (n' <> n); (* equivalent concepts have been joined above *)
	l_cover := c'::!l_cover
      done;
      !l_cover in
    s_done := Concepts.add c_ext !s_done;
    if upper_cover <> [] then (
      res := (c, upper_cover) :: !res
    )
  done;
  concepts, !res)

let generate_concepts_by_core
      ~(mode : [`Lin|`Quad]) ~only_cores
      ?max_level ?max_vars
      ?(min_supp = 1) ?(max_size = max_int)
      ~show_extent ~injective (* injective mapping for computing extensions *)
      (g : pattern) : concepts_by_core list
  = Common.prof "Generation.generate_patterns_by_core" (fun () ->
  let inter_patternset = ref Patternset.empty in (* all seen intersection pattern canonical reprs *)
  let proj_patternset = ref Patternset.empty in (* all seen projected pattern canonical reprs *)
  let ht_patterns : (Patterns.pattern_repr, concepts_by_core) Hashtbl.t = Hashtbl.create 101 in
  let print_level i = prerr_endline ("\n// concept patterns at level " ^ string_of_int i) in
  let rec generate_levelwise i level next_level =
    match level with
    | [] ->
      if next_level <> [] && max_level <> Some i
      then begin
	print_level (i+1);
	generate_levelwise (i+1) next_level []
      end
    | p1::level1 ->
      let next_level =
	(match mode with `Lin -> [g] | `Quad -> level (* NOT level1 to allow auto-intersection *))
	|> List.fold_left
	  (fun next_level p2 ->
	     (Patterns.intersection p1 p2)
	     |> List.fold_left
	      (fun next_level p_inter ->
	        let renaming_inter, repr_inter = p_inter#remove_duplicate_nodes in
		if (match max_vars with None -> false | Some m -> p_inter#nb_vars > m) (* oversized pattern *)
		   || Patternset.mem repr_inter !inter_patternset (* pattern already produced *)
		then next_level
		else begin
                  assert (p_inter#is_connected);
		  prerr_string "P"; flush stderr; (* new inter pattern *)
		  inter_patternset := Patternset.add repr_inter !inter_patternset;
		  let retracts, (good_core,core,join_sx,join_intents,join_inv_ext) =
		    get_core_and_retracts ~only_cores ~show_extent ~injective ~min_supp ~max_size g proj_patternset p_inter in
		  let core_proj = Patterns.projection core p_inter in
		  let p_proj = Patterns.projection join_sx p_inter in
		  if good_core then
		    begin
		      let core_renaming, core_repr = core_proj#canonical_repr in
		      let canonized_core =
			let l_rank_var =
			  core_renaming#fold_rank
			    (fun res rank (_,lx) -> assert (lx<>[]); (rank, List.hd lx)::res)
			    [] in
			let sorted_l_rank_var = List.sort Stdlib.compare l_rank_var in (* sorting by rank *)
			List.map snd sorted_l_rank_var in
		      try (* if already seen that core *)
			let other_cbc = Hashtbl.find ht_patterns core_repr in
			(* completing other_pat with p_proj *)
			if join_sx <> core then
                          join_with_cbc (canonized_core, p_proj, join_intents, join_inv_ext) other_cbc
		      with Not_found ->
			prerr_string "c"; flush stderr; (* new core *)
			Hashtbl.add ht_patterns core_repr
			  { core_id = 0; (* to be defined below *)
			    core_vars = canonized_core;
			    pattern = p_proj;
			    intents = join_intents;
			    inverted_extent = join_inv_ext }
		    end;
                  core_proj :: List.rev_append retracts next_level (* adding core and retracts to next level for intersection *)
		  end)
	      next_level)
	  next_level in
      generate_levelwise i level1 next_level
  in
  (try
     int2exn (fun () ->
       let core, _ = Patterns.core_vars g in
       let g = Patterns.projection (Intset.elements core) g in
       print_level 1;
       generate_levelwise 1 [g] [];
       prerr_endline "\n// no more patterns")
    with CtrlC -> ());
  let l_cbc =
    Hashtbl.fold
      (fun repr_core cbc res -> cbc::res)
      ht_patterns []
    |> List.sort
	 (fun cbc1 cbc2 -> Stdlib.compare
			     (List.length cbc1.core_vars, cbc1.pattern#nb_vars)
			     (List.length cbc2.core_vars, cbc2.pattern#nb_vars)) in
  let _ = (* numbering cbcs *)
    List.fold_left
      (fun i cbc -> cbc.core_id <- i; i+1)
      1 l_cbc in
  l_cbc)
