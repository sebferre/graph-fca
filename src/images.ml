
open Graphics
open Patterns

let to_pattern (n, m, img : int * int * color array array) : Patterns.pattern = Common.prof "Images.to_pattern" (fun () ->
  let gen_var = new gen_var in
  let var = Array.make_matrix n m 0 in
  let get_var i j =
    let x = var.(j).(i) in
    if x <> 0
    then x
    else
      let x = gen_var#get in
      var.(j).(i) <- x;
      x
  in
  let p = new Patterns.pattern in
  for i = 0 to n-1 do
    for j = 0 to m-1 do
      if img.(j).(i) <> white then
	begin
	  if i < n-1 && img.(j).(i+1) <> white then
	    p#add_edge "h" [Var (get_var i j); Var (get_var (i+1) j)];
	  if j < m-1 && img.(j+1).(i) <> white then
	    p#add_edge "v" [Var (get_var i j); Var (get_var i (j+1))]
	end
    done
  done;
  p)


let from_pattern (p : pattern) : int * int * color array array = Common.prof "Images.from_pattern" (fun () ->
  let rec aux coord queue =
    match queue with
    | [] -> coord
    | (x, (i,j))::queue1 ->
      let edges = p#var_edges x in
      let coord2, queue2 =
	List.fold_left
	  (fun (coord2, queue2) edge_ctx ->
	    let x2, i2, j2 =
	      match edge_ctx with
	      | ("h", ([], [Var x2])) -> x2, i+1, j
	      | ("h", ([Var x2], [])) -> x2, i-1, j
	      | ("v", ([], [Var x2])) -> x2, i, j+1
	      | ("v", ([Var x2], [])) -> x2, i, j-1
	      | _ -> assert false in
	    if List.mem_assoc x2 coord2
	    then (coord2, queue2)
	    else
	      let x_pos = (x2, (i2,j2)) in
	      (x_pos::coord2, x_pos::queue2))
	  (coord,queue1) edges in
      aux coord2 queue2
  in
  let coord =
    let x_pos = (List.hd p#var_list, (0,0)) in
    aux [x_pos] [x_pos] in
  let imin, imax, jmin, jmax =
    match coord with
    | [] -> assert false
    | (x,(i,j))::coord1 ->
      List.fold_left
	(fun (imin,imax,jmin,jmax) (x,(i,j)) ->
	  (min i imin, max i imax, min j jmin, max j jmax))
	(i,i,j,j) coord1 in
  let n = imax - imin + 1 in
  let m = jmax - jmin + 1 in
  let img = Array.make_matrix m n white in
  List.iter
    (fun (x,(i,j)) ->
      try img.(j-jmin).(i-imin) <- black
      with _ -> assert false)
    coord;
  n, m, img)

