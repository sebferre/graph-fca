
module Intmap = Intmap.M
module Intreln = Intreln.Intmap
module Intset = Intset.Intmap

exception TODO

let rec list_take_rest n l =
  match n, l with
  | 0, _ -> [], l
  | _, x::xs ->
    let l1, l2 = list_take_rest (n-1) xs in
    x::l1, l2
  | _, [] -> invalid_arg "list_take_rest"

let rec list_filter_by_mask mask l =
  match mask, l with
  | _, [] -> []
  | m::mask1, x::l1 ->
    if m
    then x::list_filter_by_mask mask1 l1
    else list_filter_by_mask mask1 l1
  | _ -> invalid_arg "Patterns.list_filter_by_mask: lists have not same length"
    
let rec list_has_duplicate = function
  | [] -> false
  | [_] -> false
  | x::l -> List.mem x l || list_has_duplicate l
  
module Val = Domain.Nominal(Domain.String)

let string_of_val (v : Val.t) : string =
  let cursor = Printer.cursor_of_formatter Format.str_formatter in
  Ipp.once (Domain.print_eof Val.print) v cursor ();
  Format.flush_str_formatter ()


type var = int
type node = Var of var | Val of Val.t

let node_to_string = function
  | Var v -> "X" ^ string_of_int v
  | Val v -> string_of_val v
                              
type attr = string

type args = node list
type args_ctx = node list * node list

type edge = attr * args

let edge_to_string (a,args) =
  a ^ String.concat "" (List.map (fun arg -> " " ^ node_to_string arg) args)
  
let args_of_ctx (x : node) (ll,rr : args_ctx) : args = List.rev ll @ x :: rr
let ctx_of_args (lr : args) : (node * args_ctx) list =
  let rec aux ll = function
    | [] -> []
    | x::rr -> (x,(ll,rr)) :: aux (x::ll) rr
  in
  aux [] lr

class gen_var =
object (self)
  val mutable cpt = 0
  method get : var = cpt <- cpt+1; cpt
end
  
type descr = (attr * args_ctx) list
type pattern_repr = (attr * args list) list

class var_partition (llx : var list list) =
  let sz, nb = List.fold_left (fun (sz,nb) lx -> sz + List.length lx, nb+1) (0,0) llx in
object (self : 'self)
  val size : int = sz
  val nbparts : int = nb
  val var_rank : int Intmap.t =
    let m, _ =
      List.fold_left
	(fun (m,rank) lx ->
	 List.fold_left (fun m x -> Intmap.set x rank m) m lx,
	 rank+1)
	(Intmap.empty,1) llx in
    m
  val rank_vars : (int * var list) Intmap.t =
    let m, _ =
      List.fold_left
	(fun (m,rank) lx ->
	 Intmap.set rank (List.length lx, lx) m,
	 rank+1)
	(Intmap.empty, 1) llx in
    m
  (* INV: (x,r) IN var_rank IFF (r,(k,xs)) IN rank_vars AND x IN xs *)
  (* INV: (r,(k,xs)) IN rank_vars IMPLIES k=|xs| *)
  (* INV: (1,_) IN rank_vars *)
  (* INV: (r,(k,xs)) IN rank_vars IMPLIES (r+k,_) in rank_vars AND FOR ALL 0 < j < k: (r+j,_) NOT IN rank_vars *)

  method is_unit : bool = nbparts = 1
  method is_discrete : bool = nbparts = size

  method var_rank : int Intmap.t = var_rank
					  
  method rank_of_var (x : var) : int =
    try Intmap.get x var_rank with _ -> assert false
  method var_of_rank (rank : int) : var =
    try List.hd (snd (Intmap.get rank rank_vars)) with _ -> assert false
  method vars_of_rank (rank : int) : var list =
    try snd (Intmap.get rank rank_vars) with _ -> assert false

  method fold_rank : 'a. ('a -> int -> int * var list -> 'a) -> 'a -> 'a =
    fun f init -> Intmap.fold f init rank_vars
					       
  method discretize (rank : int) (x : var) : 'self =
    let k,lx = try Intmap.get rank rank_vars with _ -> assert false in
    let rank_x = rank + k - 1 in
    let ly = List.filter ((<>) x) lx in
    assert (List.mem x lx);
    {< size = size;
       nbparts = nbparts+1;
       var_rank = Intmap.set x rank_x var_rank;
       rank_vars = Intmap.set rank_x (1,[x]) (Intmap.set rank (k-1,ly) rank_vars);
       >}

  method partition (rank : int) (parts : var list list) : 'self =
    let _, nbparts, var_rank, rank_vars =
      List.fold_left
	(fun (new_rank,nbparts,var_rank,rank_vars) lx_part ->
	 let k_part = List.length lx_part in
	 new_rank + k_part,
	 (if new_rank=rank then nbparts else nbparts+1),
	 (if new_rank=rank
	  then var_rank
	  else List.fold_left
		 (fun var_rank x -> Intmap.set x new_rank var_rank)
		 var_rank lx_part),
	 Intmap.set new_rank (k_part,lx_part) rank_vars)
	(rank,nbparts,var_rank,rank_vars) parts in
    {< size=size; nbparts=nbparts; var_rank=var_rank; rank_vars=rank_vars >}

  method to_string : string =
    let ls =
      Intmap.fold
	(fun res rank (k,lx) -> String.concat "-" (List.map string_of_int lx) :: res)
	[] rank_vars in
    String.concat "/" (List.rev ls)
end

module Permutation =
  struct
    type t = var Intmap.t
    (* INV: a bijective mapping between vars where domain = range *)

    let apply (p : t) (x : var) : var = try Intmap.get x p with _ -> assert false
		 
    let to_string (p : t) : string =
      p
      |> Intmap.fold (fun l x y -> (x,y)::l) []
      |> List.sort Stdlib.compare
      |> List.map (fun (x,y) -> string_of_int x ^ "-" ^ string_of_int y)
      |> String.concat ", "

    let orbits (p : t) : var_partition =
      let fm : (var, var list) Find_merge.hashtbl =
	new Find_merge.hashtbl ~init_val:[] ~merge_val:(@) in
      Intmap.iter
	(fun x _ -> fm#replace x [x])
	p;
      Intmap.iter
	(fun x y ->
	 if x <> y then ignore (fm#merge [x;y]))
	p;
      let llx =
	fm#fold
	  (fun _ lx res -> lx::res)
	  [] in
      new var_partition llx

  end

module Retract =
  struct
    type t = var Intmap.t (* var -> var *)
    (* INV: range is included in domain, and inverse map contains identity *)

    let domain (r : t) : Intset.t =
      Intmap.fold
	(fun res x y -> Intset.add x res)
	Intset.empty r

    let range (r : t) : Intset.t =
      Intmap.fold
	(fun res x y -> Intset.add y res)
	Intset.empty r

    let apply_var (r : t) (x : var) : var =
      try Intmap.get x r with _ -> assert false
    let apply_retract (r : t) (r1 : t) : t =
      Common.prof "Patterns.Retract.apply_retract" (fun () ->
      Intmap.fold
	(fun rr1 x y ->
	 let rx, ry = apply_var r x, apply_var r y in
	 if rx=ry && Intmap.mem rx rr1
	 then rr1 (* avoiding to overwrite existing map *)
	 else Intmap.set rx ry rr1)
	Intmap.empty r1)
		  
    let empty : t = Intmap.empty

    let add (x : var) (y : var) (r : t) : t = Intmap.set x y r

(*    let compose (r1 : t) (r2 : t) : t =
      Common.prof "Patterns.Retract.compose" (fun () ->
      let rec aux r x xs =
	if Intset.mem x xs
	then
	  let z = try Intmap.get x r with _ -> x in
	  z, r
	else
	  let y1 = try Intmap.get x r1 with _ -> assert false in
	  let y2 = try Intmap.get x r2 with _ -> assert false in
	  if y1=x && y2=x then
	    let z = try Intmap.get x r with _ -> x in
	    z, Intmap.set x z r
	  else if y1=x (* y2<>x *) then
	    let z, r = aux r y2 (Intset.add x xs) in
	    z, Intmap.set x z r
	  else (* y1<>x *)
	    let z, r = aux r y1 (Intset.add x xs) in
	    let r = if y2=x then r else aux2 r y2 Intset.empty z in
	    z, Intmap.set x z r
      and aux2 r x xs z =
	if Intset.mem x xs
	then r
	else
	  let y1 = try Intmap.get x r1 with _ -> assert false in
	  let y2 = try Intmap.get x r2 with _ -> assert false in
	  let r = if y1=x then r else aux2 r y1 (Intset.add x xs) z in
	  let r = if y2=x then r else aux2 r y2 (Intset.add x xs) z in
	  Intmap.set x z r
      in
      let r =
	Intmap.fold
	  (fun r x _ ->
	   if Intmap.mem x r
	   then r
	   else snd (aux r x Intset.empty))
	  Intmap.empty r1 in
      r) *)

    let compose (r1 : t) (r2 : t) : t =
      Common.prof "Patterns.Retract.compose" (fun () ->
      Intmap.map
        (fun x y ->
          try Some (Intmap.get y r2)
          with Not_found -> assert false)
        r1)
      
    let to_string (r : t) : string =
      let ls =
	Intmap.fold
	  (fun res x y ->
	   if x = y
	   then string_of_int x :: res
	   else (string_of_int x ^ ">" ^ string_of_int y) :: res)
	  [] r in
      String.concat " " (List.rev ls)
  end
    
  
class pattern =
object (self)
  val edges_by_attr : (attr, args list) Hashtbl.t = Hashtbl.create 13
  val edges_by_var : (var, descr) Hashtbl.t = Hashtbl.create 13

  method to_string : string =
    let buf = Buffer.create 1003 in
    Hashtbl.iter
      (fun a l_args ->
        List.iter
          (fun args ->
            Buffer.add_string buf (edge_to_string (a,args));
            Buffer.add_string buf ", ")
          l_args)
      edges_by_attr;
    Buffer.contents buf
                                            
  method mem_edge (a : attr) (args : args) : bool =
    try
      let args_list = Hashtbl.find edges_by_attr a in
      List.mem args args_list
    with _ -> false
    
  method add_edge (a : attr) (args : args) = Common.prof "Patterns.pattern#add_edge" (fun () ->
    (* assumes edges are not added several times *)
    let args_list = try Hashtbl.find edges_by_attr a with _ -> [] in
    if not (List.mem args args_list)
    then begin
      Hashtbl.replace edges_by_attr a (args :: args_list);
      List.iter
	(fun (arg,args_ctx) ->
	  match arg with
	  | Var x ->
	    let edges_ctx = try Hashtbl.find edges_by_var x with _ -> [] in
	    Hashtbl.replace edges_by_var x ((a,args_ctx)::edges_ctx)
	  | Val _ -> ())
	(ctx_of_args args)
    end)

  val var_label : (var,string) Hashtbl.t = Hashtbl.create 13
  method set_label (x : var) (l : string) : unit = Hashtbl.replace var_label x l
  method get_label (x : var) : string option = try Some (Hashtbl.find var_label x) with _ -> None (* "X" ^ string_of_int x *)

  method var_edges (x : var) : descr = try Hashtbl.find edges_by_var x with _ -> []
  method attr_args_list (a : attr) : args list = try Hashtbl.find edges_by_attr a with _ -> []
    
  method var_list : var list = Common.prof "Patterns.pattern#var_list" (fun () ->
    Hashtbl.fold (fun x _ res -> x::res) edges_by_var [])
  method attr_list : attr list = Common.prof "Patterns.pattern#attr_list" (fun () ->
    Hashtbl.fold (fun a _ res -> a::res) edges_by_attr [])

  method edges : edge list = Common.prof "Patterns.pattern#edges" (fun () ->
    Hashtbl.fold
      (fun a args_list res ->
	List.fold_left
	  (fun res args -> (a,args)::res)
	  res args_list)
      edges_by_attr [])

  method nb_vars : int = Hashtbl.length edges_by_var
  method nb_edges : int = Hashtbl.fold (fun a args_list res -> List.length args_list + res) edges_by_attr 0
    
  method neighbours (x : var) : var list = Common.prof "Patterns.pattern#neighbours" (fun () ->
    List.fold_left
      (fun res (a,(ll,rr)) ->
	List.fold_left
	  (fun res -> function
	  | Var x2 -> x2::res
	  | Val _ -> res)
	  res (List.rev_append ll rr))
      [] (self#var_edges x))

  method connected_components : var list list = Common.prof "Patterns.pattern#connected_components" (fun () ->
    let rec aux ccs cc queue others =
      match queue with
      | [] ->
	let new_ccs = cc::ccs in
	let new_others = List.filter (fun x -> not (List.mem x cc)) others in
	( match new_others with
	| [] -> new_ccs
	| x::others1 -> aux new_ccs [] [x] others1 )
      | x::queue1 ->
	if List.mem x cc
	then aux ccs cc queue1 others
	else
	  let neighbours = self#neighbours x in
	  aux ccs (x::cc) (List.rev_append neighbours queue1) others
    in
    match self#var_list with
    | [] -> []
    | x::others -> aux [] [] [x] others)

  method is_connected : bool =
    List.length self#connected_components = 1

  (* returns a canonical representation, and a list of automorphisms *)
  (* the canonical representation is made of a bijective partition (mapping from vars to ranks) and a pattern representation based on ranks *)
  method canonical_automorphisms : (var_partition * pattern_repr) * Permutation.t list =
    Common.prof "Patterns.pattern#canonical_automorphisms" (fun () ->
    let partition_refiner (p : var_partition) : var_partition =
      Common.prof "Patterns.pattern#canonical_automorphisms/partition_refiner" (fun () ->
      let recode_args p args =
	args
	|> List.map
	     (function
	       | Var x -> Var (p#rank_of_var x)
	       | (Val _ as arg) -> arg) in
      let recode_edges_ctx p edges_ctx =
	Common.prof "Patterns.pattern#canonical_automorphisms/recode_edges_ctx" (fun () ->
	edges_ctx
	|> List.map
	     (fun (a,(ll,rr)) ->
	      (a, (recode_args p ll, recode_args p rr)))
	|> Common.sort_equiv Stdlib.compare
	|> List.map List.hd) in
      let rec propagate_constraints (p : var_partition) : var_partition =
	let changed, p =
	  p#fold_rank
	    (fun (changed,p) rank (k,lx) ->
	     if k = 1
	     then (changed,p)
	     else
	       let lxd =
		 List.map
		   (fun x ->
		    let edges_ctx = self#var_edges x in
		    (x, recode_edges_ctx p edges_ctx))
		   lx in
	       let sorted_equiv_lxd =
		 Common.sort_equiv
		   (fun (x1,edges1) (x2,edges2) -> Stdlib.compare edges1 edges2)
		   lxd in
	       match sorted_equiv_lxd with
	       | [] -> assert false
	       | [_] -> changed, p
	       | _ ->
		  let parts =
		    List.map (List.map fst) sorted_equiv_lxd in
		  true, p#partition rank parts)
	    (false,p) in
	if changed
	then propagate_constraints p
	else p in
      propagate_constraints p) in
    let cell_selector (p : var_partition) : (int * var list) option =
      Common.prof "Patterns.pattern#canonical_automorphisms/cell_selector" (fun () ->
      (* looking for the smallest non-discrete rank *)
      p#fold_rank
	(fun res rank (k,lx) ->
	 if k = 1 then res
	 else
	   match res with
	   | None -> Some (rank,lx)
	   | Some (best_rank,best_lx) ->
	      if k < best_rank then Some (rank,lx) else res)
	None) in
    let certificate (p : var_partition) : pattern_repr =
      Common.prof "Patterns.pattern#canonical_automorphisms/certificate" (fun () ->
      assert (p#is_discrete);
      let repr : pattern_repr =
	Common.prof "Patterns.pattern#canonical_automorphisms/certificate/repr" (fun () ->
	let m = p#var_rank in (* to have quicker access than method call *)
	Hashtbl.fold
	  (fun a largs repr ->
	   let largs_repr =
	     List.rev_map
	       (fun args ->
		List.map
		  (fun n ->
		   match n with
		   | Var x ->
		      let r = Intmap.get x m in
		      if r=x then n else Var r
		   | (Val _ as n) -> n)
		  args)
	       largs in
	   (a, List.sort Stdlib.compare largs_repr) :: repr)
	  edges_by_attr []) in
      List.sort Stdlib.compare repr) in
    let make_automorphism (phi : var_partition) (lambda : var_partition) : Permutation.t =
      assert (phi#is_discrete);
      assert (lambda#is_discrete);
      (* compute phi o lambda-1 *)
      phi#fold_rank
	(fun res rank ->
	 function
	 | (1,[x]) -> Intmap.set x (lambda#var_of_rank rank) res
	 | _ -> assert false)
	Intmap.empty in
    let rec tree_search seen_certif_p best_certif_p autos p =
      let p = partition_refiner p in
      match cell_selector p with
      | None -> (* p is discrete *)
	 let certif = certificate p in
	 (try (* existing certificate *)
	     let seen_p = List.assoc certif seen_certif_p in
	     let auto = make_automorphism seen_p p in
             let autos =
               if List.mem auto autos
               then autos
               else auto::autos in
	     Some seen_p, [], seen_certif_p, best_certif_p, autos
	   with Not_found -> (* new certificate *)
	     let best_certif_p =
	       let best_certif = fst best_certif_p in
	       if best_certif = [] || certif < best_certif
	       then (certif,p)
	       else best_certif_p in
	     None, [p], (certif,p)::seen_certif_p, best_certif_p, autos)
      | Some (rank, ly) ->
	 let backjump, first_p, seen_certif_p, best_certif_p, autos =
	   List.fold_left
	     (fun (backjump,first_p,seen_certif_p,best_certif_p,autos) y ->
	      match backjump with
	      | None ->
		 let backjump, new_first_p, seen_certif_p, best_certif_p, autos =
		   tree_search seen_certif_p best_certif_p autos
			       (p#discretize rank y) in
		 backjump, new_first_p@first_p, seen_certif_p, best_certif_p, autos
	      | Some p_ref -> (* backjumping until p_ref first path *)
		 backjump, first_p, seen_certif_p, best_certif_p, autos)
	     (None,[],seen_certif_p,best_certif_p,autos) ly in
	 let backjump =
	   match backjump with
	   | Some p_ref when not (List.memq p_ref first_p) -> backjump
	   | _ -> None in
	 backjump, first_p, seen_certif_p, best_certif_p, autos
    in
    let p0 = new var_partition [self#var_list] in
    let backjump, _first_p, _seen_certif_p, (best_certif, best_p), autos =
      tree_search [] ([], new var_partition []) [] p0 in
    assert (backjump = None);
    (best_p, best_certif), autos)

  (* returns the canonical representation (see above) *)
  method canonical_repr : var_partition * pattern_repr =
    let canonical, _autos = self#canonical_automorphisms in
    canonical

  (* returns a symmetric retract, if possible, from an automorphism *)
  method symmetric_retract_from_automorphism (auto : Permutation.t) : Retract.t option =
    Common.prof "Patterns.pattern#symmetric_retract_from_automorphism" (fun () ->
    (* assuming pattern is connected *)
    let p = Permutation.orbits auto in
    let rec loop (r : Retract.t) (dom : Intset.t) (ran : Intset.t) (queue : Intset.t) : Retract.t option =
      try
	let x = Intset.choose queue in
	let r, dom, queue =
	  let orbit = p#rank_of_var x in
	  let ly = p#vars_of_rank orbit in
	  List.fold_left
	    (fun (r,dom,queue) y -> Retract.add y x r, Intset.add y dom, Intset.remove y queue)
	    (r,dom,queue) ly in
	let ran = Intset.add x ran in
	let check_retract =
	  let descr = self#var_edges x in
	  let rec process_args = function
	    | [] -> true, false, []
	    | arg :: args ->
	       let all_in_dom, all_in_ran, r_args = process_args args in
	       match arg with
	       | Var z ->
		  let r_z =
		    if Intset.mem z dom
		    then Retract.apply_var r z
		    else z in
		  all_in_dom && Intset.mem z dom,
		  all_in_ran && Intset.mem z ran,
		  Var r_z :: r_args
	       | Val _ -> all_in_dom, all_in_ran, arg::r_args
	  in
	  List.for_all
	    (fun (a,(ll,rr)) ->
	     let all_in_dom_ll, all_in_ran_ll, r_ll = process_args ll in
	     let all_in_dom_rr, all_in_ran_rr, r_rr = process_args rr in
	     let all_in_dom = all_in_dom_ll && all_in_dom_rr in
	     let all_in_ran = all_in_ran_ll && all_in_ran_rr in
	     if all_in_dom && not all_in_ran
	     then List.mem (a,(r_ll,r_rr)) descr (* checking that retracted edge exists *)
			   (* valid because x is retracted on x *)
	     else true) (* edge not checked w.r.t. x *)
	    descr in
	if check_retract
	then
	  let queue =
	    let ln = self#neighbours x in
	    List.fold_left
	      (fun queue n -> if Intset.mem n dom then queue else Intset.add n queue)
	      queue ln in
	  loop r dom ran queue
	else None
      with Not_found -> Some r
    in (* end loop *)
    loop
      Retract.empty
      Intset.empty
      Intset.empty
      (Intset.singleton (p#var_of_rank 1))) (* choosing a starting var *)

  (* returns the canonical representation, and the subset of vars on which the pattern can be retracted, if possible, according to all automorphisms *)
  method symmetric_retract : (var_partition * pattern_repr) * Intset.t option =
    Common.prof "Patterns.pattern#symmetric_retract" (fun () ->
    let canonical, autos = self#canonical_automorphisms in
    (* TODO: select maximally fine-grained autos? *)    
    let retracts =
      Common.mapfilter self#symmetric_retract_from_automorphism autos in
    match retracts with
    | [] -> (canonical, None)
    | r1::_ ->
       let composed_retract =
         let rec aux r1 = (* saturating application of retracts *)
           let r2 = List.fold_left Retract.compose r1 retracts in
           if r2 = r1 then r1 else aux r2 in
         aux r1 in
       let range = Retract.range composed_retract in
       (canonical, Some range))

  (* remove duplicate nodes according to symmetric retracts, and returns the canonical reprensentation of the maximally retracted pattern *)
  method remove_duplicate_nodes : var_partition * pattern_repr = (* returns canonical repr *)
    Common.prof "Patterns.pattern#remove_duplicate_nodes" (fun () ->
    let canonical, sx_opt = self#symmetric_retract in
    match sx_opt with
    | None -> canonical
    | Some sx ->
       (* removing all nodes not in sx *)
       Common.prof "Patterns.patterns#remove_duplicate_nodes/removing_nodes" (fun () ->
       let check_args args =
	 List.for_all
	   (function
	     | Var x -> Intset.mem x sx
	     | Val _ -> true)
	   args in
       edges_by_attr
       |> Hashtbl.iter
	    (fun a largs ->
	     largs
	     |> List.filter check_args
	     |> Hashtbl.replace edges_by_attr a);
       edges_by_var
       |> Hashtbl.iter
	    (fun x descr ->
	     if not (Intset.mem x sx)
	     then Hashtbl.remove edges_by_var x
	     else
	       descr
	       |> List.filter
		    (fun (a,(ll,rr)) ->
		     check_args ll && check_args rr)
	       |> Hashtbl.replace edges_by_var x);
       var_label
       |> Hashtbl.iter
	    (fun x label ->
	     if not (Intset.mem x sx)
	     then Hashtbl.remove var_label x));
       self#remove_duplicate_nodes)

      
  (*  val mutable canonical_repr : pattern_repr option = None *)
  (* deprecated *)
(*
  method canonical_repr : (var,var) Hashtbl.t (* canonizing renaming *) * pattern_repr (* canonical repr *) =
   Common.prof "Patterns.pattern#canonical_repr" (fun () ->
    let recode_args var_rank args =
      List.map
	(fun arg ->
	  match arg with
	  | Var x -> Var (Hashtbl.find var_rank x)
	  | Val _ -> arg)
	args in
    let recode_edges_ctx var_rank edges_ctx = Common.prof "Patterns.pattern#canonize/recode_edges_ctx" (fun () ->
      List.map List.hd (* removing duplicates *)
	(Common.sort_equiv Stdlib.compare
	   (List.map
	      (fun (a,(ll,rr)) ->
		(a, (recode_args var_rank ll, recode_args var_rank rr)))
	      edges_ctx))) in
    let propagate_constraints var_rank rank_vars =
      let has_changed = ref true in
      Common.prof "Patterns.pattern#canonize/while" (fun () ->
      while !has_changed do
	has_changed := false;
	Array.iteri
	  (fun rank -> function
	  | None -> ()
	  | Some (1,_) -> ()
	  | Some (k,lx) -> (* more than 1 object in cluster *)
	    let lxd =
	      List.map
		(fun x ->
		  let edges_ctx = self#var_edges x in
		  (x, recode_edges_ctx var_rank edges_ctx))
		lx in
	    let sorted_equiv_lxd = Common.prof "Patterns.pattern#canonize/sort_equiv" (fun () ->
	      Common.sort_equiv (fun (x1,edges1) (x2,edges2) -> Stdlib.compare edges1 edges2) lxd) in
	    match sorted_equiv_lxd with
	    | [] -> assert false
	    | [_] -> () (* no refinement *)
	    | _ -> (* refinement *)
	      has_changed := true;
	      ignore (List.fold_left
			(fun new_rank lxd_part ->
			  let lx_part = List.map fst lxd_part in
			  let k_part = List.length lx_part in
			  if new_rank <> rank then
			    List.iter (fun x_part -> Hashtbl.replace var_rank x_part new_rank) lx_part;
			  rank_vars.(new_rank) <- Some (k_part, lx_part);
			  new_rank + k_part)
			rank sorted_equiv_lxd)
	  )
	  rank_vars
      done) in
(*
    let force_discrete_partition n var_rank rank_vars =
      let rank = ref 1 in
      while !rank <= n do
	match rank_vars.(!rank) with
	| None -> assert false
	| Some (1,_) -> incr rank
	| Some (_,[]) -> assert false
	| Some (k,x::lx1) ->
	   (* doing the split *)
	   let rank1 = !rank+1 in
	   rank_vars.(!rank) <- Some (1,[x]);
	   rank_vars.(rank1) <- Some (k-1,lx1);
	   List.iter
	     (fun x1 -> Hashtbl.replace var_rank x1 rank1)
	     lx1;
	   (* propagating constraints *)
	   propagate_constraints var_rank rank_vars;
	   incr rank
      done
    in
 *)
    let var_list = self#var_list in
    let n = List.length var_list in
    let var_rank : (var, int) Hashtbl.t = Hashtbl.create (1 + n) in
    let rank_vars : (int * var list) option array = Array.make (1+n) None in
    (* normal inits *)
    List.iter (fun x -> Hashtbl.add var_rank x 1) var_list;
    rank_vars.(1) <- Some (n, var_list);
    propagate_constraints var_rank rank_vars;
    (* tentative, to avoid invalid retracts, like cycles *)
    (*
    let is_retract =
      (* building mapping from vars to first var of rank *)
      let var_var = Hashtbl.create n in
      Hashtbl.iter
	(fun v rank ->
	 match (try rank_vars.(rank) with _ -> assert false) with
	 | Some (_,x::_) -> Hashtbl.add var_var v x
	 | _ -> assert false)
	var_rank;
      (* checking that every retracted edge is an edge *)
      Hashtbl.fold
	(fun a largs is_retract ->
	 if is_retract
	 then
	   List.for_all
	     (fun args ->
	      let retracted_args = recode_args var_var args in
	      List.mem retracted_args largs)
	     largs
	 else false)
	edges_by_attr true in
    if not is_retract then
      begin
	prerr_string "#";
	force_discrete_partition n var_rank rank_vars
      end;
     *)
    let compact_n, to_compact, rev_vars =
      Common.fold_for
	(fun rank (compact_rank, to_compact, rev_vars) ->
	  match rank_vars.(rank) with
	  | None -> (compact_rank, to_compact, rev_vars)
	  | Some (_,[]) -> assert false
	  | Some (_,x::_) -> (compact_rank+1, (rank,compact_rank+1)::to_compact, x::rev_vars))
	1 n (0,[],[]) in
    Hashtbl.iter (* compacting var_rank *)
      (fun x rank -> Hashtbl.replace var_rank x
	(try List.assoc rank to_compact with _ -> assert false))
      var_rank;
    let recoded_edges_by_var =
      List.rev_map
	(fun x ->
	  let edges_ctx = self#var_edges x in
	  (Hashtbl.find var_rank x, recode_edges_ctx var_rank edges_ctx))
	rev_vars in
    var_rank, recoded_edges_by_var)

  method canonize = Common.prof "Patterns.pattern#canonize" (fun () ->
    let var_rank, recoded_edges_by_var = self#canonical_repr in
    (* TODO: recode node labels *)
    Hashtbl.clear edges_by_var;
    List.iter (fun (x,edges) -> Hashtbl.add edges_by_var x edges) recoded_edges_by_var;
    Hashtbl.clear edges_by_attr;
    List.iter
      (fun (x,edges_ctx) ->
	List.iter
	  (fun (a,args_ctx) ->
	    match args_ctx with
	    | [], rr ->
	      let args = try Hashtbl.find edges_by_attr a with _ -> [] in
	      Hashtbl.replace edges_by_attr a ((Var x::rr)::args)
	    | _ -> ())
	  edges_ctx)
      recoded_edges_by_var;
    var_rank, recoded_edges_by_var)
 *)
 
  (* includes [pat2] into pattern by unifying [lv] (vars of this pattern) and [lv2] (vars of [pat2]) *)
  method join_with (lx, lx2 : var list * var list) (pat2 : pattern) : (var * var) list = Common.prof "Patterns.pattern#join_with" (fun () ->
    let _, subst =
      let lx2x = List.combine lx2 lx in
      let x_max = List.fold_left max 0 self#var_list in (* maximum var of this pattern *)
      List.fold_left
	(fun (i,subst) x2 ->
	  try (i, (x2, List.assoc x2 lx2x)::subst)
	  with Not_found -> (i+1, (x2,i)::subst))
	(x_max+1, [])
	pat2#var_list in
    let rename = function
      | Var x2 -> Var (try List.assoc x2 subst with Not_found -> assert false)
      | Val v -> Val v in
    List.iter
      (fun (a,args2) -> self#add_edge a (List.map rename args2))
      pat2#edges;
    subst)

end

let projection (vars : var list) (p : pattern) : pattern = Common.prof "Patterns.projection" (fun () ->
  let projected = new pattern in
  List.iter
    (fun (a,args) ->
      if (List.for_all (function Var x -> List.mem x vars | _ -> true) args)
      then projected#add_edge a args)
    p#edges;
  projected)

type extent = var list * Intreln.t

let extent_is_empty (vars,rel) = Intreln.is_empty rel

let extent_cardinal (vars,rel) = Intreln.cardinal rel

let project_extent lv (vars,rel) =
  let mask = List.map (fun v -> List.mem v lv) vars in
  let proj_vars = List.filter (fun v -> List.mem v lv) vars in
  let proj_rel = Intreln.project mask rel in
  (proj_vars, proj_rel)

let injective_extent ?mask (vars,rel) =
  let rel2 =
    Intreln.fold
      (fun rel2 sol ->
	if list_has_duplicate (match mask with None -> sol | Some mask -> list_filter_by_mask mask sol)
	then rel2
	else Intreln.add sol rel2)
      (Intreln.empty (Intreln.dim rel)) rel in
  (vars,rel2)

let inverted_extent (vars,rel) : (var * Intset.t) list =
  let llo =
    Intreln.fold
      (fun llo sol -> List.map2 (fun lo x -> Intset.add x lo) llo sol)
      (List.map (fun _ -> Intset.empty) vars) rel in
  List.map2 (fun x lo -> (x,lo)) vars llo


exception Empty_ext

let extension_join ~injective (vars1, ext1 : extent) (vars2, ext2 : extent) : extent = Common.prof "Patterns.extension_join" (fun () ->
  let vars2' = List.filter (fun v2 -> not (List.mem v2 vars1)) vars2 in
  let vars = vars1 @ vars2' in
  let ext =
    let s_vars2 = List.map string_of_int vars2 in
    let s_vars2' = List.map string_of_int vars2' in
    Intreln.extension (List.length vars2')
      (fun sol1 ->
	Intreln.fold_restr
	  (List.rev_map2 (fun x y -> (string_of_int x, y)) vars1 sol1)
	  s_vars2
	  (fun ext2' m ->
	    let sol2' = List.map (fun x2 -> List.assoc x2 m) s_vars2' in
	    if injective && List.exists (fun y2 -> List.mem y2 sol1) sol2'
	    then ext2'
	    else Intreln.add sol2' ext2')
	  (Intreln.empty (List.length vars2'))
	  ext2)
      ext1 in
  if Intreln.is_empty ext then raise Empty_ext;
  (vars,ext))

let extension_join_list ~injective (vars, ext : extent) (vars_ext_list : extent list) : extent list = Common.prof "Patterns.extension_join_list" (fun () ->
  let l_related, l_unrelated =
    List.partition (fun (vars1,ext1) -> List.exists (fun v -> List.mem v vars1) vars) vars_ext_list in
  let joined_vars_ext =
    List.fold_left
      (fun vars_ext vars1_ext1 -> extension_join ~injective vars_ext vars1_ext1)
      (vars,ext) l_related in
  joined_vars_ext :: l_unrelated)

let extension_list ~injective (* of *) (p1 : pattern) (* in *) (p2 : pattern) : extent list = Common.prof "Patterns.extension" (fun () ->
  try
    let edges = p1#edges in
    List.fold_left
      (fun vars_ext_list (a,args1) ->
	let vars = (* list of vars in args1 *)
	  List.fold_right (fun arg vars -> match arg with Var x -> x::vars | Val _ -> vars) args1 [] in
	let ext = (* matches for args1' vars in p2 *)
	  List.fold_left
	    (fun ext args2 ->
	      let sol, ok =
		List.fold_right2
		  (fun arg1 arg2 (sol,ok) ->
		    match arg1, arg2 with
		    | Var x1, Var x2 -> x2::sol, ok
		    | Val v1, Val v2 -> sol, Val.subsumed v1 v2
		    | _ -> sol, false)
		  args1 args2 ([],true) in
	      if ok then Intreln.add sol ext else ext)
	    (Intreln.empty (List.length vars))
	    (p2#attr_args_list a) in
	if Intreln.is_empty ext then raise Empty_ext;
	extension_join_list ~injective (vars,ext) vars_ext_list)
      [] edges
  with Empty_ext -> [])
    
module SetSet =
struct
  type tree = { mutable present : bool; children : (int,tree) Hashtbl.t }
  type ctx = Root | X of int * tree * ctx | Path of int list * tree * ctx
  type t = S of tree * ctx | SPath of int list * tree * ctx
  (* invariant: each int-path from root should be in decreasing ordering *)
  (* invariant: each int-path from root must be free of any duplicate *)

  let ctx_of_ss = function
    | S (_,ctx) -> ctx
    | SPath (path,parent,ctx) -> Path (path,parent,ctx)

  let rec ctx_depth = function
    | Root -> 0
    | X (i,parent,ctx) -> 1 + ctx_depth ctx
    | Path (path,parent,ctx) -> List.length path + ctx_depth ctx
 
  let empty () = { present = false; children = Hashtbl.create 7 }
  let init () = S (empty (), Root)

  let rec finish ss = Common.prof "Patterns.SetSet.finish" (fun () ->
    match ss with
    | S (node, ctx) -> node.present <- true
    | SPath (path, parent, ctx) -> finish_down parent path)
  and finish_down parent = function
    | [] -> parent.present <- true
    | i::path ->
      let child =
	try Hashtbl.find parent.children i
	with Not_found ->
	  let child = empty () in
	  Hashtbl.add parent.children i child;
	  child in
      finish_down child path
	
  type position = OnPath | Above of int (* how many steps upward for insertion *)

  let rec position ?(above = 0) i = function
    | Root -> Above above
    | X (j, parent, ctx) ->
      if i < j then Above above
      else if j=i then OnPath
      else position ~above:(above+1) i ctx
    | Path ([], parent, ctx) -> assert false
    | Path (j::path, parent, ctx) ->
      if i > j then position ~above:(above + 1 + List.length path) i ctx
      else if i = j || List.mem i path then OnPath
      else Above (List.fold_left (fun res j -> if j < i then res+1 else res) 0 path)

  exception Oversized

  let rec extend ?max_size i ss = Common.prof "Patterns.SetSet.extend" (fun () ->
    let ctx = ctx_of_ss ss in
    match position i ctx with
    | OnPath -> ss
    | Above n ->
      ( match max_size with
      | Some ms when ctx_depth ctx >= ms -> raise Oversized
      | _ -> extend_down (extend_up i [] n ss) ))
  and extend_up i path_down n ss =
    if n = 0
    then (i::path_down, ss)
    else
      match ss with
      | S (tree, X (j,parent,ctx)) ->
	extend_up i (j::path_down) (n-1) (S (parent,ctx))
      | SPath (path,parent,ctx) ->
	let l = List.length path in
	if l <= n
	then extend_up i (path@path_down) (n-l) (S (parent,ctx))
	else
	  let path1, path2 = list_take_rest (l-n) path in
	  (i::path2@path_down, SPath (path1, parent, ctx))
      | _ -> assert false
  and extend_down (path_down, ss) =
    match ss with
    | S (node, ctx) ->
      ( match path_down with
      | [] -> ss
      | i::l ->
	(try
	   let child = Hashtbl.find node.children i in
	   extend_down (l, S (child, X (i, node, ctx)))
	 with Not_found ->
	   SPath (path_down, node, ctx)) )
    | SPath (path,parent,ctx) ->
      SPath (path@path_down, parent, ctx)

  let rec fold_increasing_card (f : 'a -> int list -> 'a) (init : 'a) (tree : tree) : 'a = Common.prof "SetSet.fold_increasing_card" (fun () ->
    (* invariant: [int list] are sorted in increasing order *)
    (* invariant: [f] is applied to sets in increasing cardinality *)
    let res = ref init in
    let q = Queue.create () in
    Queue.add ([], tree) q;
    (try
       while true do
	 let rev_path, node = Queue.take q in
	 if node.present then res := f !res rev_path;
	 Hashtbl.iter (fun i child -> Queue.add (i::rev_path, child) q) node.children
       done
     with Queue.Empty -> ());
    !res)

(*
  let singleton (vars : var list) : tree =
    let ss = init () in
    let ss =
      List.fold_left
	(fun ss v -> extend v ss)
	ss vars in
    finish ss;
    match ss with
    | S (tree, Root) -> tree
    | _ -> assert false
*)

end

let fold_extension ~projset ?max_size
    (f : 'a -> var array -> 'a) (init : 'a)
 (* of *) (p1 : pattern) (* in *) (p2 : pattern) : SetSet.tree * var list * 'a = Common.prof "Patterns.fold_extension" (fun () ->
  (* the hashtbl maps p1-vars to indexes in the array *)
  (* the array contains p2-vars, and constitutes a solution to p1-query in p2-graph *)
  (* the var list as result contains the list of p1-vars to interpret solutions *)
  (* BEWARE: not valid with loop edges in p1 *)
  let eval_edge (bound_vars : var list) (a,args as edge) : 'sp * (int * int * [`KeyVar|`NewVar|`Val] list * var list * edge) = Common.prof "Patterns.fold_extension/eval_edge" (fun () ->
    let mask, nb_key, nb_new, new_bound_vars =
      List.fold_right
	(fun arg (mask, nb_key, nb_new, new_bound_vars) ->
	  match arg with
	  | Var x ->
	    if List.mem x bound_vars
	    then (`KeyVar::mask, 1+nb_key, nb_new, new_bound_vars)
	    else (`NewVar::mask, nb_key, 1+nb_new, x::new_bound_vars)
	  | Val _ -> (`Val::mask, nb_key, nb_new, new_bound_vars))
	args ([],0,0,bound_vars) in
    let sp = (nb_new, - nb_key) in (* least new vars, most keyvars *)
    (sp, (nb_key, nb_new, mask, new_bound_vars, edge)))
  in
  let rec planify (bound_vars : var list) (edges : edge list) : var list * (int * int * [`KeyVar|`NewVar|`Val] list * edge) list = Common.prof "Patterns.fold_extension/planify" (fun () ->
    match edges with
    | [] -> bound_vars, []
    | edges ->
      let eval_edges = List.map (eval_edge bound_vars) edges in
      let (sp, (nb_key, nb_new, mask, bound_vars, best_edge)), other_eval_edges =
	Common.extract_min (fun (sp1,_) (sp2,_) -> Stdlib.compare sp1 sp2) eval_edges in
      let other_edges = List.map (fun (_, (_,_,_,_,edge)) -> edge) other_eval_edges in
      let bound_vars, sorted_edges = planify bound_vars other_edges in
      bound_vars, (nb_key,nb_new,mask,best_edge) :: sorted_edges)
  in
  let preprocess (bound_vars : var list) sorted_edges = Common.prof "Patterns.fold_extension/preprocess" (fun () ->
    let n = List.length bound_vars in
    (* building a map from bound variables to their index in the solution array *)
    let var_index = Hashtbl.create (n+1) in
    ignore (List.fold_left (fun i x -> Hashtbl.add var_index x i; i+1) 0 bound_vars);
    (* the solution array to be modified by 'aux' *)
    let sol = Array.make n 0 in
    let processed_edges =
      List.map
	(fun (nb_key, nb_new, mask, (a,args)) ->
	  let key_indexes = Array.make nb_key 0 in
	  let new_indexes = Array.make nb_new 0 in
	  ignore
	    (List.fold_left
	       (fun (i,j,mask) arg ->
		 match mask, arg with
		 | `KeyVar::mask1, Var x -> key_indexes.(i) <- Hashtbl.find var_index x; (i+1,j,mask1)
		 | `NewVar::mask1, Var x -> new_indexes.(j) <- Hashtbl.find var_index x; (i,j+1,mask1)
		 | `Val::mask1, Val _ -> (i,j,mask1)
		 | _ -> assert false) (* TODO: heterogeneous modes for a same predicate *)
	       (0,0,mask) args);
	  let ht = Hashtbl.create 13 in
	  List.iter
	    (fun args2 ->
	      let key_vals = Array.make nb_key 0 in
	      let new_vals = Array.make nb_new 0 in
	      try
		ignore
		  (List.fold_left2
		     (fun (i,j,mask) arg arg2 ->
		       match mask, arg, arg2 with
		       | `KeyVar::mask1, Var _, Var x2 -> key_vals.(i) <- x2; (i+1,j,mask1)
		       | `NewVar::mask1, Var _, Var x2 -> new_vals.(j) <- x2; (i,j+1,mask1)
		       | `Val::mask1, Val v1, Val v2 when Val.subsumed v1 v2 -> (i,j,mask1)
		       | _ -> raise Not_found (* mismatch *))
		     (0,0,mask) args args2);
		let l = try Hashtbl.find ht key_vals with Not_found -> [] in
		Hashtbl.replace ht key_vals (new_vals :: l)
	      with _ -> ()) (* in case of mismatch *)
	    (p2#attr_args_list a);
	  if nb_new = 0
	  then (key_indexes, `Check ht)
	  else (key_indexes, `Extend (ht, new_indexes)))
	sorted_edges in
    var_index, sol, processed_edges)
  in
  let bound_vars, sorted_edges = planify [] p1#edges in
  let var_index, sol, preprocessed_edges = preprocess bound_vars sorted_edges in
  let rec aux ss acc = function
    | [] ->
      if projset then SetSet.finish ss;
      f acc sol
    | (keyvar_indexes, keyvars)::edges ->
      let keyvar_vals = Array.map (fun i -> sol.(i)) keyvar_indexes in
      ( match keyvars with
      | `Check ht ->
	if Hashtbl.mem ht keyvar_vals
	then aux ss acc edges
	else acc
      | `Extend (ht, newvar_indexes) ->
	let n = Array.length newvar_indexes in
	let vals_list = try Hashtbl.find ht keyvar_vals with Not_found -> [] in
	List.fold_left
	  (fun acc vals ->
	    (* updating 'sol' *)
	    try
	      let ss1 = ref ss in
	      for j = 0 to n-1 do
		let index = newvar_indexes.(j) in
		let v = vals.(j) in
		sol.(index) <- v;
		if projset then ss1 := SetSet.extend ?max_size v !ss1
	      done;
	      aux !ss1 acc edges
	    with SetSet.Oversized -> acc)
	  acc vals_list
      )
  in
  Common.prof "Patterns.fold_extension/iteration" (fun () ->
    let ss = SetSet.init () in
    let res = aux ss init preprocessed_edges in
    match ss with
    | SetSet.S (tree,SetSet.Root) -> tree, bound_vars, res
    | _ -> assert false))


let extension (* of *) (p1 : pattern) (* in *) (p2 : pattern) : extent = Common.prof "Patterns.extension" (fun () ->
  let n = List.length p1#var_list in
  let _, vars, rel =
    (*print_endline (p1#to_string);*)
    fold_extension ~projset:false
      (fun rel sol -> Intreln.add (Array.to_list sol) rel)
      (Intreln.empty n) (* assumes p1 is connected *)
      p1 p2 in
  (vars, rel))

(*
let canonized_connected_components p = Common.prof "Patterns.canonized_connected_components" (fun () ->
  let ccs = p#connected_components in
  List.fold_left
    (fun res cc ->
      let p = projection cc p in
      p#canonize;
      p :: res)
    [] ccs)
*)

let intersection (p1 : pattern) (p2 : pattern) : pattern list = Common.prof "Patterns.intersection" (fun () ->
  let gen_var = new gen_var in
  let h_align : (var * var, var) Hashtbl.t = Hashtbl.create 101 in
  let align x1 x2 = Common.prof "Patterns.intersection.align" (fun () ->
    (try Hashtbl.find h_align (x1,x2)
     with Not_found ->
       let x = gen_var#get in
       Hashtbl.add h_align (x1,x2) x;
       x)) in
  let intersection_args args1 args2 = Common.prof "Patterns.intersection.intersection_args" (fun () ->
    let rev_args =
      List.rev_map2
	(fun arg1 arg2 ->
	  match arg1, arg2 with
	  | Var x1, Var x2 -> Var (align x1 x2)
	  | Val v1, Val v2 -> Val (Val.similarity v1 v2)
	  | _ -> invalid_arg "Patterns.intersection/intersection_args")
	args1 args2 in
    List.fold_left
      (fun (args, vars) arg ->
	match arg with
	| Var x -> arg::args, x::vars
	| _ -> arg::args, vars)
      ([],[]) rev_args)
  in
  let attrs1 = p1#attr_list in
  let attrs2 = p2#attr_list in
  let common_attrs = List.filter (fun a -> List.mem a attrs2) attrs1 in
  (* for each var, the list of edges having it as argument, along with the two edges' args the edge was built from *)
  let var_edges : (var, edge Bintree.t) Hashtbl.t = Hashtbl.create 101 in
  let find_merge : (edge, edge list) Find_merge.hashtbl = new Find_merge.hashtbl ~init_val:[] ~merge_val:(@) in
  List.iter
    (fun a ->
      let args_list1 = p1#attr_args_list a in
      let args_list2 = p2#attr_args_list a in
      List.iter
	(fun args1 ->
	  List.iter
	    (fun args2 ->
	     try
	       let args, args_vars = intersection_args args1 args2 in
	       let edge = (a,args) in
	       let adjacent_edge_set : edge Bintree.t = (* collecting existing edges with a common variable *)
		 List.fold_left
		   (fun res arg_var -> Bintree.union res (try Hashtbl.find var_edges arg_var with Not_found -> Bintree.empty))
		   Bintree.empty args_vars in
	       let adjacent_edge_list = Bintree.elements adjacent_edge_set in
	       let root_edge = find_merge#merge (edge::adjacent_edge_list) in
	       let edges = find_merge#find root_edge in
	       find_merge#replace root_edge (edge::edges);
	       List.iter
		 (fun arg_var ->
		  let edges = try Hashtbl.find var_edges arg_var with Not_found -> Bintree.empty in
		  Hashtbl.replace var_edges arg_var (Bintree.add edge edges))
		 args_vars
	     with _ -> (* incompatible pair of args *)
	       ())
	    args_list2)
	args_list1)
    common_attrs;
  find_merge#fold
    (fun _ edges res ->
      let p = new pattern in
      List.iter (fun (a,args) -> p#add_edge a args) edges;
      p :: res)
    [])


module CoreState =
  struct
    type cat = Core | NonCore | Undet
    type domain = Intset.t (* vars *)
    type var_state =
      { cat : cat;
	card : int;
	domain : domain; (* vars *)
      }
    type t =
      { map : var_state Intmap.t; (* var -> var_state *)
	core_vars : Intset.t;
	core_card : int;
      }

    let cat_of_var (v : var) (card : int) (domain : domain) : cat =
      if card=1
      then if Intset.mem v domain then Core else NonCore
      else Undet

    let var_state_of_domain (v : var) (domain : domain) : var_state =
      let card = Intset.cardinal domain in
      let cat = cat_of_var v card domain in
      {cat; card; domain}
	     
    let create ~(init_domains : (var * domain) list) (vars : Intset.t) : t =
      Intset.fold
	(fun cstate v ->
	 let domain = try List.assoc v init_domains with _ -> vars in
	 let vstate = var_state_of_domain v domain in
	 if vstate.cat = Core
	 then
	   { map = Intmap.set v vstate cstate.map;
	     core_vars = Intset.add v cstate.core_vars;
	     core_card = 1 + cstate.core_card }
	 else
	   { cstate with map = Intmap.set v vstate cstate.map })
	{ map = Intmap.empty;
	  core_vars = Intset.empty;
	  core_card = 0 }
	vars

    let create_identity (vars : Intset.t) : t =
      let init_domains =
	Intset.fold (fun res v -> (v, Intset.singleton v)::res) [] vars in
      create ~init_domains vars
	
    let set_domain (v : var) (domain : domain) (cstate : t) : t =
      let vstate = var_state_of_domain v domain in
      if vstate.cat = Core && not (Intset.mem v cstate.core_vars)
      then { map = Intmap.set v vstate cstate.map;
	     core_vars = Intset.add v cstate.core_vars;
	     core_card = 1 + cstate.core_card }
      else { cstate with map = Intmap.set v vstate cstate.map }

    let get_var_state (v : var) (cstate : t) : var_state =
      try Intmap.get v cstate.map
      with _ -> assert false

    let get_domain (v : var) (cstate : t) : domain =
      try (Intmap.get v cstate.map).domain
      with _ -> assert false

    let get_cat (v : var) (cstate : t) : cat =
      try (Intmap.get v cstate.map).cat
      with _ -> assert false

    let get_core (cstate : t) : int * Intset.t =
      assert (Intset.cardinal cstate.core_vars = cstate.core_card);
      cstate.core_card, cstate.core_vars
      (*Intmap.fold
	(fun (card,vars as res) v v_state ->
	 if v_state.cat = Core
	 then (card+1, Intset.add v vars)
	 else res)
	(0,Intset.empty) cstate*)

    let fold (f : 'a -> var -> var_state -> 'a) (init : 'a) (cstate : t) : 'a =
      Intmap.fold f init cstate.map
  end
    
exception Empty_domain
	    
let propagate_edge_constraints (pattern : pattern) (cstate : CoreState.t) (changed_vars : Intset.t) : CoreState.t (* raises Empty_domain *)
  = Common.prof "Patterns.propagate_edge_constraints" (fun () ->
  let changed_vars = ref changed_vars in
  let res = ref cstate in
  while not (Intset.is_empty !changed_vars) do
    let x = Intset.choose !changed_vars in
    changed_vars := Intset.remove x !changed_vars;
    let descr = pattern#var_edges x in
    descr |>
      List.iter
	(fun (a,ctx) ->
	 let x_args = args_of_ctx (Var x) ctx in
	 let arity = List.length x_args in
	 let l_args = pattern#attr_args_list a in
	 let new_domains =
	   List.map (fun arg -> Intset.empty) x_args in
	 let new_domains =
	   l_args |>
	     List.fold_left
	       (fun new_domains args ->
		let matching =
		  List.length args = arity &&
		    List.for_all2
		      (fun arg x_arg ->
		       match arg, x_arg with
		       | Var y, Var x_y -> Intset.mem y (CoreState.get_domain x_y !res)
		       | Val v, Val x_v -> Val.subsumed x_v v
		       | _ -> false)
		      args x_args in
		if matching
		then
		  List.map2
		    (fun arg new_domain ->
		     match arg with
		     | Var y -> Intset.add y new_domain
		     | _ -> new_domain)
		    args new_domains
		else new_domains)
	       new_domains in
	 List.iter2
	   (fun x_arg new_domain ->
	    match x_arg with
	    | Var v ->
	       let domain_v = CoreState.get_domain v !res in
	       if Intset.is_empty domain_v then raise Empty_domain
	       else if Intset.subset domain_v new_domain then () (* no change *)
	       else
		 begin
		   changed_vars := Intset.add v !changed_vars;
		   res := CoreState.set_domain v new_domain !res;
		 end
	    | Val _ -> () (* implicit domain for vals *))
	   x_args new_domains)
  done;
  assert (Intset.is_empty !changed_vars);
  !res)

  
let core_vars ?(init_domains : (var * Intset.t) list = []) (pattern : pattern) : Intset.t * (var * var) list = Common.prof "Patterns.core_vars" (fun () ->
    let all_vars = pattern#var_list in
    let set_all_vars = List.fold_right Intset.add all_vars Intset.empty in
    let cstate = CoreState.create ~init_domains set_all_vars in
    let changed_vars = set_all_vars in
    let rec search cstate changed_vars (best : int * Intset.t * CoreState.t) : int * Intset.t * CoreState.t = (* best: core size, core vars, cstate = mapping *)
      let cstate_opt =
	try Some (propagate_edge_constraints pattern cstate changed_vars)
	with Empty_domain -> None in
      match cstate_opt with
      | Some cstate ->
	 let card, core = CoreState.get_core cstate in
	 let card0, core0, cstate0 = best in
	 if card >= card0
	 then best (* looking for smallest retract *)
	 else
	   let undet_choice : (int * var * Intset.t) option = (* choosing least undetermined var *)
	     cstate
	     |> CoreState.fold
		  (fun res v v_state ->
		   let open CoreState in
		   if v_state.cat = Undet
		   then
		     match res with
		     | None -> Some (v_state.card, v, v_state.domain)
		     | Some (card0, v0, domain_v0) ->
			if v_state.card < card0
			then Some (v_state.card, v, v_state.domain)
			else res
		   else res)
		  None in
	   ( match undet_choice with
	     | None -> (* no undetermined var *)
		if card < card0
		then card, core, cstate
		else best
	     | Some (_,v,domain_v) ->
		let core_vars, undet_vars =
		  domain_v
		  |> Intset.fold
		       (fun (core_vars, undet_vars) x ->
			match CoreState.get_cat x cstate with
			| CoreState.Core -> (x::core_vars, undet_vars)
			| CoreState.Undet -> (core_vars, x::undet_vars)
			| CoreState.NonCore -> (core_vars, undet_vars))
		       ([],[]) in
		let best =
		  core_vars
		  |> List.fold_left
		       (fun best x ->
			let cstate = CoreState.set_domain v (Intset.singleton x) cstate in
			let changed_vars = Intset.singleton v in
			search cstate changed_vars best)
		       best in
		let best =
		  undet_vars
		  |> List.fold_left
		       (fun best x ->
			let cstate = CoreState.set_domain v (Intset.singleton x) cstate in
			let cstate = CoreState.set_domain x (Intset.singleton x) cstate in
			let changed_vars = Intset.add x (Intset.singleton v) in
			search cstate changed_vars best)
		       best in
		best )
      | None -> best
    in
    let core_card, core_vars, core_cstate =
      let cstate_id = CoreState.create_identity set_all_vars in
      search cstate changed_vars (Intset.cardinal set_all_vars, set_all_vars, cstate_id) in
    let map =
      CoreState.fold
	(fun res v v_state ->
	 let domain = v_state.CoreState.domain in
	 assert (Intset.cardinal domain = 1);
	 (v, Intset.choose domain)::res)
	[] core_cstate in
    core_vars, map)
