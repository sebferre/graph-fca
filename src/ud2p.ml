(* Conversion of UD (Universal Dependencies) files to gfca programs. *)

let output_word ~nolemma chout sid word_pos word lemma postag parent_pos dep =
  (*  let word_pos = Str.global_replace (Str.regexp "[.]") "-" word_pos in*)
  let dep =
    try String.sub dep 0 (String.index dep ':')
    with _ -> dep in
  if nolemma
  then
    Printf.fprintf
      chout
      "S%dW%s : [ %s ], "
      sid word_pos postag
  else
    Printf.fprintf
      chout
      "S%dW%s : [ lemma \"%s\", %s ], "
      sid word_pos lemma postag;
  Printf.fprintf
    chout
    "%s S%dW%s S%dW%s,\n"
    dep sid parent_pos sid word_pos

let process ~max_sentences ~nopunct ~nolemma chin chout =
  let lid = ref 0 in
  try
    let sid = ref 1 in
    output_string chout "\n:-\n";
    while true do
      let line = input_line chin in
      incr lid;
      if line = "" then begin
	  incr sid;
	  if !sid > max_sentences then raise End_of_file
	end
      else if line.[0] = '#' then ()
      else
	match Str.split (Str.regexp_string "\t") line with
	| [word_pos; word; lemma; postag; _; _; parent_pos; dep; _; _] ->
	   if (nopunct && postag = "PUNCT")
	      || String.contains word_pos '.'
	   then ()
	   else output_word ~nolemma chout !sid word_pos word lemma postag parent_pos dep
	| _ -> prerr_endline ("wrong line: " ^ line)
    done
  with
  | End_of_file ->
     output_string chout "end _.\n" (* pseudo-edge for syntactic reason *)
  | exn -> prerr_int !lid; prerr_string ": "; prerr_endline (Printexc.to_string exn)

let main () =
  let max_sentences = ref max_int in
  let nopunct = ref false in
  let nolemma = ref false in
  Arg.parse
    [ "-n", Arg.Set_int max_sentences, "only convert max N sentences";
      "-nopunct", Arg.Set nopunct, "ignore PUNCT words";
      "-nolemma", Arg.Set nolemma, "ignore word lemmas";
    ]
    (fun filename -> ())
    "./ud2p [-n <int>] [-nopunct] [-nolemma] < input > output";
  process ~max_sentences:!max_sentences
	  ~nopunct:!nopunct
	  ~nolemma:!nolemma
	  stdin stdout

let _ = main ()
	     
