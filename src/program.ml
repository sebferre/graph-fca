
type pred = string
type var = string

let _ = Random.self_init ()

type 'a choice = (float * 'a) list
let choose (choice : 'a choice) : 'a =
  let p = Random.float 1. in
  let rec aux w_sum = function
    | [] -> assert false
    | [(_,x)] -> x
    | (w,x)::l ->
      let w_sum = w_sum +. w in
      if p < w_sum
      then x
      else aux w_sum l
  in
  aux 0. choice
  
type program = statement list
and statement =
| Pattern of pattern
| PatternRule of head * pattern
| DescrRule of head * descr
| RelRule of head * rel
| STreeRule of head * stree
and head = pred * var list (* (p,lv) *)
and pattern =
| Atom of pred * arg list
| Descr of descr
| Rel of rel
| STree of stree
| Subj of arg * descr
| And of pattern list
| Choice of pattern choice
and descr =
| DAtom of pred * arg list
| DRel of rel * arg
| DAnd of descr list
| DChoice of descr choice
and rel =
| RAtom of pred * arg list
| RSeq of rel list
| RAnd of rel list
| RChoice of rel choice
and stree = (* sequential tree: sequence + composition *)
| TNode of pred * arg list * descr option * stree list
| TAnd of stree list
| TChoice of stree choice
and arg =
| AAtom of node
| AAnon of descr * var option
| AAnd of arg list
| AChoice of arg choice
and node =
| Anonymous
| Var of var
| Val of Patterns.Val.t

let regexp_ws = "\\([\t\r\n ]\\|%[^\r\n]*[\r\n]*\\)" (* white spaces and comments *)
let regexp_ws_plus = regexp_ws ^ "+"
let regexp_ws_star = regexp_ws ^ "*"

let parse_ws_plus = dcg [ _ = match regexp_ws_plus as "ws+" -> () ]
let parse_ws_star = dcg [ _ = match regexp_ws_star as "ws*" -> () ]
let parse_pred = dcg [ s = match "[A-Za-z][A-Za-z0-9'_-]*" as "attribute" -> s ]
let parse_var = dcg [ s = match "[A-Za-z][A-Za-z0-9'_-]*" as "var" -> s ]
let parse_string = dcg [ "\""; s = match "[^\"]*" as "string"; "\"" -> s ]
let parse_int = dcg [ s = match "[0-9]+" as "int" -> s ]
let parse_prob = dcg [ s = match "[.][0-9]+" as "prob" -> float_of_string s ]

let parse_infix op = dcg [ _ = parse_ws_star; 'op; _ = parse_ws_star -> () ]
let parse_brackets left p right = dcg [ 'left; _ = parse_ws_star; x = p; _ = parse_ws_star; 'right -> x ]

let parse_comma = dcg [ _ = parse_infix "," -> () ]
let parse_colon = dcg [ _ = parse_infix ":" -> () ]
let parse_ampersand = dcg [ _ = parse_infix "&" -> () ]
let parse_alt = dcg [ _ = parse_infix "|" -> () ]
let parse_dash = dcg [ _ = parse_infix "-" -> () ]
let parse_if = dcg [ _ = parse_infix ":-" -> () ]
let parse_dot = dcg [ _ = parse_infix "." -> () ]

let parse_and p = dcg [ 
| l = LIST1 p SEP parse_comma -> l
|   -> [] ]
let rec parse_choice p = dcg [
| x = p; ( _ = parse_ws_plus; ( f = parse_prob; _ = parse_alt; ch = parse_choice p -> (f,x)::ch
			      | _ = parse_alt; l = LIST1 p SEP parse_alt ->
			        let f = 1. /. (float (1 + List.length l)) in
			        List.map (fun x -> (f,x)) (x::l) )
         |  -> [(1.,x)] ) ]

let rec parse_statement = dcg [
| ":-"; _ = parse_ws_star; pat = parse_pattern; _ = parse_dot -> Some (Pattern pat)
| h = parse_head; _ = parse_infix ":-"; b = parse_pattern; _ = parse_dot -> Some (PatternRule (h,b))
| h = parse_brackets "[" parse_head "]"; _ = parse_infix ":-"; d = parse_descr_block; _ = parse_dot -> Some (DescrRule (h,d))
| h = parse_brackets "<" parse_head ">"; _ = parse_infix ":-"; r = parse_rel_block; _ = parse_dot -> Some (RelRule (h,r))
| h = parse_brackets "[<" parse_head ">]"; _ = parse_infix ":-"; t = parse_stree_block; _ = parse_dot -> Some (STreeRule (h,t))
| _ = parse_ws_plus -> None ]
and parse_head = dcg [
| p = parse_pred; lv = MANY [ _ = parse_ws_plus; v = parse_var -> v ] -> (p,lv) ]
and parse_pattern = dcg [
| ch = parse_choice parse_pattern_and -> (match ch with [(_,p)] -> p | _ -> Choice ch) ]
and parse_pattern_and = dcg [
| l = parse_and parse_pattern_atom -> (match l with [p] -> p | _ -> And l) ]
and parse_pattern_atom = dcg [
| p = parse_brackets "(" parse_pattern ")" -> p
| p = parse_pred; args = parse_args -> Atom (p, args)
| d = parse_descr_block -> Descr d
| r = parse_rel_block -> Rel r
| t = parse_stree_block -> STree t
| a = parse_arg; pat = parse_pattern_arg a -> pat ]
and parse_pattern_arg a = dcg [
| _ = parse_colon; d = parse_descr_block -> Subj (a,d)
| _ = parse_ws_plus; r = parse_rel_block; _ = parse_ws_plus; a2 = parse_arg -> Subj (a, DRel (r,a2)) ]
and parse_descr_block = dcg [
| d = parse_brackets "[" parse_descr "]" -> d ]
and parse_descr = dcg [
| ch = parse_choice parse_descr_and -> (match ch with [(_,d)] -> d | _ -> DChoice ch) ]
and parse_descr_and = dcg [
| l = parse_and parse_descr_atom -> (match l with [d] -> d | _ -> DAnd l) ]
and parse_descr_atom = dcg [
| d = parse_brackets "(" parse_descr ")" -> d
| r = parse_rel_block; _ = parse_ws_plus; a = parse_arg -> DRel (r,a)
| p = parse_pred; args = parse_args -> DAtom (p,args) ]
and parse_rel_block = dcg [
| r = parse_brackets "<" parse_rel ">" -> r ]
and parse_rel = dcg [
| ch = parse_choice parse_rel_and -> (match ch with [(_,r)] -> r | _ -> RChoice ch) ]
and parse_rel_and = dcg [
| l = parse_and parse_rel_seq -> (match l with [r] -> r | _ -> RAnd l) ]
and parse_rel_seq = dcg [
| l = LIST1 parse_rel_atom SEP parse_dash -> (match l with [r] -> r | _ -> RSeq l) ]
and parse_rel_atom = dcg [
| r = parse_brackets "(" parse_rel ")" -> r
| p = parse_pred; args = parse_args -> RAtom (p,args) ]
and parse_stree_block = dcg [
| t = parse_brackets "[<" parse_stree ">]" -> t ]
and parse_stree = dcg [
| ch = parse_choice parse_stree_and -> (match ch with [(_,t)] -> t | _ -> TChoice ch) ]
and parse_stree_and = dcg [
| l = parse_and parse_stree_node -> (match l with [t] -> t | _ -> TAnd l) ]
and parse_stree_node = dcg [
| p = parse_pred; args = parse_args;
  d_opt = OPT [ _ = parse_colon; d = parse_descr_block -> Some d ] ELSE None;
  _ = parse_ws_plus;
  l = OPT [ l = parse_brackets "[<" parse_stree_children ">]" -> l ] ELSE [] -> TNode (p,args,d_opt,l) ]
and parse_stree_children = dcg [
| l = LIST1 parse_stree SEP parse_dash -> l ]
and parse_args = dcg [
| l = MANY [ _ = parse_ws_plus; arg = parse_arg -> arg ] -> l ]
and parse_arg_choice = dcg [
| ch = parse_choice parse_arg -> (match ch with [(_,a)] -> a | _ -> AChoice ch) ]
and parse_arg = dcg [
| l = LIST1 parse_arg_atom SEP parse_ampersand -> (match l with [a] -> a | _ -> AAnd l) ]
and parse_arg_atom = dcg [
| a = parse_brackets "(" parse_arg_choice ")" -> a
| d = parse_descr_block; v_opt = OPT [ "@"; v = parse_var -> Some v ] ELSE None -> AAnon (d,v_opt)
| n = parse_node -> AAtom n ]
and parse_node = dcg [
| "_" -> Anonymous
| "[]" -> Anonymous
| v = parse_var -> Var v
| v = Patterns.Val.parse -> Val v ]


let parse_file (ch : in_channel) : program = Common.prof "Program.parse_file" (fun () ->
  let cursor = Matcher.cursor_of_channel ch in
  let _, rev_l =
    Dcg.once_fold
      (fun acc -> function None -> acc | Some x -> x::acc)
      [] parse_statement
      () cursor in
  close_in ch;
  List.rev rev_l)


class scope gen_var =
object
  val obj_var : (string,Patterns.var) Hashtbl.t = Hashtbl.create 101
  method create_var (obj : string) : Patterns.var =
    let x = gen_var#get in
    Hashtbl.add obj_var obj x;
    x
  method get_var (obj : string) =
    try Some (Hashtbl.find obj_var obj)
    with Not_found -> None

  val bindings : (var, Patterns.node) Hashtbl.t = Hashtbl.create 13
  method bind_var v arg = Hashtbl.add bindings v arg
  method get_var_binding v =
    try Some (Hashtbl.find bindings v)
    with Not_found -> None
end

class predicate_set =
object
  val mutable set : pred Bintree.t = Bintree.empty
  method add (p : pred) : unit = set <- Bintree.add p set
  method mem (p : pred) : bool = Bintree.mem p set
end
  
class env =
object (self)
  val pattern = new Patterns.pattern
  method pattern = pattern

  val gen_var = new Patterns.gen_var

  val rules : (string * int, (string list * pattern)) Hashtbl.t = Hashtbl.create 13
  method add_rule p lv pat =
    Hashtbl.add rules (p, List.length lv) (lv,pat)
  method get_rule p arity =
    try Some (Hashtbl.find rules (p,arity))
    with Not_found -> None

  val mutable anonymous_cpt = 0
  method get_anonymous =
    anonymous_cpt <- anonymous_cpt + 1;
    "_" ^ string_of_int anonymous_cpt
      
  val mutable scopes : scope list = []
  method open_scope = scopes <- (new scope gen_var)::scopes
  method close_scope = (try scopes <- List.tl scopes with _ -> assert false)
  method private scope = (try List.hd scopes with _ -> assert false)
  method bind_var v arg = self#scope#bind_var v arg
    
  method private first_scope : 'a. (scope -> 'a option) -> 'a option =
    fun f ->
      let rec aux = function
	| None, scope::scopes -> f scope (* aux (f scope, scopes) *)
	| acc, _ -> acc in
      aux (None,scopes)
  method get_var v =
    match self#first_scope (fun scope -> scope#get_var v) with
    | Some x -> x
    | None ->
      let x = self#scope#create_var v in
      if v.[0] <> '_' then pattern#set_label x v;
      x
  method get_var_binding v =
    self#first_scope (fun scope -> scope#get_var_binding v)

  val mutable descr_preds = new predicate_set
  method descr_predicates = descr_preds
  val mutable rel_preds = new predicate_set
  method rel_predicates = rel_preds
  val mutable stree_preds = new predicate_set
  method stree_predicates = stree_preds
end


let rec run_program env = function
  | ls -> List.iter (run_statement env) ls
and run_statement env = function
  | Pattern pat ->
    env#open_scope;
    run_pattern env pat;
    env#close_scope
  | PatternRule ((p,lv),pat) ->
    env#add_rule p lv pat
  | DescrRule ((p,lv),d) ->
    let v = env#get_anonymous in
    env#add_rule p (v::lv) (Subj (AAtom (Var v), d))
  | RelRule ((p,lv),r) ->
    let v1 = env#get_anonymous in
    let v2 = env#get_anonymous in
    env#add_rule p (v1::v2::lv) (Subj (AAtom (Var v1), DRel (r, AAtom (Var v2))))
  | STreeRule ((p,lv),t) ->
    let parent = env#get_anonymous in
    let pre = env#get_anonymous in
    let child = env#get_anonymous in
    let post = env#get_anonymous in
    let args = List.map (fun v -> AAtom (Var v)) [parent; pre; child; post] in
    env#add_rule p (parent::pre::child::post::lv) (Atom (p, args))
and run_pattern env = function
  | Atom (p,args) -> run_args env (fun nodes -> run_edge env p nodes) args
  | Descr d ->
    let node = Var (env#get_anonymous) in
    run_descr env node d
  | Rel r ->
    let node1 = Var (env#get_anonymous) in
    let node2 = Var (env#get_anonymous) in
    run_rel env node1 node2 r
  | STree t ->
     let parent = Var env#get_anonymous in
     let pre = Var env#get_anonymous in
     let child = Var env#get_anonymous in
     let post = Var env#get_anonymous in
     run_stree env parent pre child post t
  | Subj (arg,d) -> run_arg env (fun node -> run_descr env node d) arg
  | And l -> List.iter (run_pattern env) l
  | Choice ch -> run_pattern env (choose ch)
and run_descr env node = function
  | DAtom (p,args) ->
     env#descr_predicates#add p;
     run_args env (fun nodes -> run_edge env p (node::nodes)) args
  | DRel (r,arg) ->
     run_arg env (fun node2 -> run_rel env node node2 r) arg
  | DAnd l -> List.iter (run_descr env node) l
  | DChoice ch -> run_descr env node (choose ch)
and run_rel env node1 node2 = function
  | RAtom (p,args) ->
     env#rel_predicates#add p;
     run_args env (fun nodes -> run_edge env p (node1::node2::nodes)) args
  | RSeq l ->
    ( match l with
    | [] -> assert false
    | [r] -> run_rel env node1 node2 r
    | r::lr ->
      let node = Var (env#get_anonymous) in
      run_rel env node1 node r;
      run_rel env node node2 (RSeq lr) )
  | RAnd l -> List.iter (run_rel env node1 node2) l
  | RChoice ch -> run_rel env node1 node2 (choose ch)
and run_stree env parent pre child post = function
  | TNode (p,args,d_opt,lt) ->
     env#stree_predicates#add p;
     run_args env (fun nodes -> run_edge env p (parent::pre::child::post::nodes)) args;
     (match d_opt with None -> () | Some d -> run_descr env child d);
     let ltpp, _, _ =
       List.fold_right
	 (fun t (ltpp,pre,post) -> ((t,pre,post)::ltpp, Var env#get_anonymous, pre))
	 lt ([], Var env#get_anonymous, post) in
     let ltpp = (* fixing first pre *)
       match ltpp with
       | [] -> []
       | (t,_,post)::l1 -> (t,pre,post)::l1 in
     List.iter
       (fun (t,pre,post) ->
	let gdchild = Var env#get_anonymous in
	run_stree env child pre gdchild post t)
       ltpp
  | TAnd l -> List.iter (run_stree env parent pre child post) l
  | TChoice ch -> run_stree env parent pre child post (choose ch)
and run_args env d args = run_arg_list env d [] args
and run_arg_list env d rev_nodes = function
  | [] -> d (List.rev rev_nodes)
  | arg::args -> run_arg env (fun node -> run_arg_list env d (node::rev_nodes) args) arg
and run_arg env d = function
  | AAtom node -> d node
  | AAnon (descr,v_opt) ->
    let node = Var (match v_opt with Some v -> v | None -> env#get_anonymous) in
    d node; run_descr env node descr
  | AAnd la -> List.iter (run_arg env d) la
  | AChoice ch -> run_arg env d (choose ch)
and run_edge env p nodes =
  let pattern_nodes =
    List.map
      (function
      | Anonymous ->
	let x = env#get_var (env#get_anonymous) in
	Patterns.Var x
      | Var v ->
	( match env#get_var_binding v with
	| Some pattern_node -> pattern_node
	| None ->
	  let x = env#get_var v in
	  Patterns.Var x)
      | Val v -> Patterns.Val v)
      nodes in
  match env#get_rule p (List.length nodes) with
  | None ->
    env#pattern#add_edge p pattern_nodes
  | Some (lv,pat) ->
    env#open_scope;
    List.iter2
      (fun v pattern_node -> env#bind_var v pattern_node)
      lv pattern_nodes;
    run_pattern env pat;
    env#close_scope

let run (p : program) : Patterns.pattern * env =
  let env = new env in
  run_program env p;
  env#pattern, env


type mapping = (string * Patterns.node) list
type meta_node = Node of Patterns.node | Unknown of string

let unknown ?(name : string option) (f : string -> 'a) : 'a =
  let u = match name with Some u -> u | None -> "X" ^ string_of_int (Random.int 1000000) in
  f u
	      
exception TODO
exception NoMatch

let rec matches_pattern (env : env) (pat : pattern) (m : mapping) (k : mapping -> bool) : bool =
  match pat with
  | Atom (p,args) ->
    matches_args env args m
      (fun lx m -> matches_edge env p lx m k)
  | Descr d ->
     unknown
       (fun u -> matches_descr env d (Unknown u) m k)
  | Rel r ->
     unknown
       (fun u1 ->
	unknown
	  (fun u2 ->
	   matches_rel env r (Unknown u1) (Unknown u2) m k))
  | STree t -> raise TODO
  | Subj (arg,d) ->
    matches_arg env arg m
      (fun x m -> matches_descr env d x m k)
  | And [] -> k m
  | And (pat::lpat) ->
    matches_pattern env pat m
      (fun m -> matches_pattern env (And lpat) m k)
  | Choice ch ->
     List.exists
       (fun (_,pat) -> matches_pattern env pat m k)
       ch
and matches_edge env (p : Patterns.attr) (lx : meta_node list) m (k : mapping -> bool) =
  let n = List.length lx in
  match env#get_rule p n with
  | Some (lv,pat) when List.length lv = n ->
     let m1, unbound_lv =
       List.fold_left2
	 (fun (m1,unbound_lv) v x ->
	  match x with
	  | Node n -> (v,n)::m1, unbound_lv
	  | Unknown u -> m1, (v,u)::unbound_lv)
	 ([],[]) lv lx in
     matches_pattern
       env pat m1
       (fun m1 ->
	let m =
	  List.fold_left
	    (fun m (v,u) -> (* TODO: case when different 'v' are associated to same 'u': check node equality *)
	     try (u, List.assoc v m1)::m with _ -> m)
	    m unbound_lv in
	k m)
  | _ ->
     (env#pattern#attr_args_list p) |>
       List.exists
	 (fun ln ->
	  try
	    let m =
	      List.fold_left2
		(fun m x n ->
		 match x with
		 | Node n0 -> if n0=n then m else raise NoMatch
		 | Unknown u ->
		    ( try
			let n0 = List.assoc u m in
			if n0 = n then m else raise NoMatch
		      with Not_found ->
			(u,n)::m ))
		m lx ln in
	    k m
	  with NoMatch -> false)
and matches_descr_opt env d_opt (x : meta_node) m (k : mapping -> bool) =
  match d_opt with
  | None -> k m
  | Some d -> matches_descr env d x m k
and matches_descr env d (x : meta_node) m (k : mapping -> bool) =
  match d with
  | DAtom (p,args) ->
    matches_args env args m
      (fun lx m -> matches_edge env p (x::lx) m k)
  | DRel (r,arg) ->
    matches_arg env arg m
      (fun x2 m -> matches_rel env r x x2 m k)
  | DAnd [] -> k m
  | DAnd (d1::ld1) ->
    matches_descr env d1 x m
      (fun m -> matches_descr env (DAnd ld1) x m k)
  | DChoice ch ->
    List.exists
      (fun (_,d) -> matches_descr env d x m k)
      ch
and matches_rel env r (x1 : meta_node) (x2 : meta_node) m (k : mapping -> bool) =
  match r with
  | RAtom (p,args) ->
    matches_args env args m
      (fun lx m -> matches_edge env p (x1::x2::lx) m k)
  | RSeq [] -> k m
  | RSeq [r1] -> matches_rel env r1 x1 x2 m k
  | RSeq (r1::lr1) ->
     unknown
       (fun u ->
	matches_rel
	  env r1 x1 (Unknown u) m
	  (fun m -> matches_rel env (RSeq lr1) (Unknown u) x2 m k))
  | RAnd [] -> k m
  | RAnd (r1::lr1) ->
    matches_rel env r1 x1 x2 m
      (fun m -> matches_rel env (RAnd lr1) x1 x2 m k)
  | RChoice ch ->
    List.exists
      (fun (_,r) -> matches_rel env r x1 x2 m k)
      ch
and matches_stree env t (parent : meta_node) (pre : meta_node) (child : meta_node) (post : meta_node) m (k : mapping -> bool) =
  match t with
  | TNode (p,args,d_opt,lt) ->
     matches_args env args m
       (fun lx m ->
	matches_edge env p (parent::pre::child::post::lx) m
	  (fun m ->
	   matches_descr_opt env d_opt child m
	     (fun m ->
	      matches_stree_children env lt child pre post m k)))
  | TAnd [] -> k m
  | TAnd (t1::lt1) ->
    matches_stree env t1 parent pre child post m
      (fun m -> matches_stree env (TAnd lt1) parent pre child post m k)
  | TChoice ch ->
    List.exists
      (fun (_,t) -> matches_stree env t parent pre child post m k)
      ch
and matches_stree_children env lt parent pre post m k =
  match lt with
  | [] -> k m
  | [t] ->
     unknown
       (fun u_child ->
	matches_stree env t parent pre (Unknown u_child) post m k)
  | t1::lt1 ->
     unknown
       (fun u_child ->
	unknown
	  (fun u_pos ->
	   matches_stree env t1 parent pre (Unknown u_child) (Unknown u_pos) m
	     (fun m -> matches_stree_children env lt1 parent (Unknown u_pos) post m k)))
and matches_args env args m (k : meta_node list -> mapping -> bool) =
  match args with
  | [] -> k [] m
  | arg1::args1 ->
    matches_arg env arg1 m
      (fun x m -> matches_args env args1 m
	(fun lx1 m -> k (x::lx1) m))
and matches_arg env arg m (k : meta_node -> mapping -> bool) =
  match arg with
  | AAtom node -> matches_node env node m k
  | AAnon (d, u_opt) ->
     unknown ?name:u_opt
	     (fun u -> matches_descr env d (Unknown u) m (k (Unknown u)))
  | AAnd l ->
     List.for_all
       (fun arg -> matches_arg env arg m k)
       l
  | AChoice ch ->
    List.exists
      (fun (_,arg1) -> matches_arg env arg1 m k)
      ch
and matches_node env node m k =
  match node with
  | Anonymous -> unknown (fun u -> k (Unknown u) m)
  | Var v ->
     (try k (Node (List.assoc v m)) m
      with Not_found -> unknown ~name:v (fun u -> k (Unknown u) m))
  | Val v -> k (Node (Patterns.Val v)) m

let matches_pred env p lx =
  matches_edge env p lx [] (fun m -> true)
    
  
