
module Set = Bintree
module Intset = Intset.Intmap
module Intreln = Intreln.Intmap

open Experiment_t

exception TODO

class pattern_store ?(excluded_predicates : string list = []) pattern =
object (self : #Cnn.store)
  method all_oids ~obs =
    List.fold_left
      (fun res oid -> Intset.add oid res)
      Intset.empty pattern#var_list
  method get_class_extension ~obs uri = Intreln.empty 1
  method get_property_extension ~obs uri = false, Intreln.empty 2
  val functor_extension : (string * int, Intreln.t) Hashtbl.t = Hashtbl.create 13
  method get_functor_extension ~obs uri arity =
    if List.mem uri excluded_predicates
    then Intreln.empty arity
    else
      try Hashtbl.find functor_extension (uri,arity)
      with Not_found ->
	let rel = Common.prof "Cnnc.get_functor_extension" (fun () ->
	  List.fold_left
	    (fun res args ->
	      let args_oids =
		List.fold_right
		  (fun arg res ->
		    match arg with
		    | Patterns.Var v -> v::res
		    | Patterns.Val _ -> res)
		  args [] in
	      if List.length args_oids <> arity
	      then res
	      else Intreln.add args_oids res)
	    (Intreln.empty arity) (pattern#attr_args_list uri)) in
	Hashtbl.add functor_extension (uri,arity) rel;
	rel

  method pattern_changed =
    Hashtbl.clear functor_extension
	
  method get_label oid =
    match pattern#get_label oid with
    | Some s -> s
    | None -> "oid" ^ string_of_int oid

  method adjacent_edges oid = Common.prof "Cnnc.adjacent_edges" (fun () ->
    List.fold_left
      (fun res (a,args_ctx) ->
	if List.mem a excluded_predicates
	then res
	else
	  let args = Patterns.args_of_ctx (Patterns.Var oid) args_ctx in
	  let oids_args =
	    List.fold_right
	      (fun arg res ->
		match arg with
		| Patterns.Var v -> v::res
		| Patterns.Val _ -> res) (* values are ignored *)
	      args [] in
	  Cnn.EdgeSet.add {Cnn.Edge.pred = Cnn.Funct (a,List.length oids_args);
			   Cnn.Edge.args = oids_args;
			   Cnn.Edge.priority = 1 }
	    res)
      Cnn.EdgeSet.empty (pattern#var_edges oid))
    
end

class ['a,'b] acc_index (f : 'b -> 'b -> 'b) (init : 'b) =
object
  val ht : ('a,'b) Hashtbl.t = Hashtbl.create 7
  method add (k : 'a) (v : 'b) : unit =
    let v0 = try Hashtbl.find ht k with _ -> init in
    Hashtbl.replace ht k (f v0 v)
  method get (k : 'a) : 'b =
    try Hashtbl.find ht k with _ -> init
  method as_list : ('a * 'b) list =
    Hashtbl.fold (fun k v res -> (k,v)::res) ht []
end
  
class classes ~program_env ~(arity : int) (lc : Experiment_t.classe list) =
object
  val seeds_c : (Cnn.oid list, Experiment_t.predicate list) Hashtbl.t = Hashtbl.create 101
    
  method of_seeds (seeds : Cnn.oid list) : Experiment_t.predicate list =
    try Hashtbl.find seeds_c seeds
    with _ ->
      let lpred =
	try
	  Common.mapfilter
	    (function
	      | `Check pred ->
	         if program_env#pattern#mem_edge pred
						 (List.map (fun v -> Patterns.Var v) seeds)
		 then Some pred
		 else None
	      | `Match pred ->
		 if Program.matches_pred
		      program_env pred
		      (List.map (fun v -> Program.Node (Patterns.Var v)) seeds)
		 then Some pred
		 else None
	      | `NotMatch pred ->
		 if Program.matches_pred
		      program_env pred
		      (List.map (fun v -> Program.Node (Patterns.Var v)) seeds)
		 then None
		 else Some ("not_" ^ pred))
	    lc
	with _ -> [] in
      Hashtbl.add seeds_c seeds lpred;
      lpred

  method predicates =
    List.map (function `Check pred -> pred | `Match pred -> pred | `NotMatch pred -> "not_" ^ pred) lc
	
  method iter (f : Experiment_t.predicate -> unit) : unit =
    List.iter
      (function
      | `Check pred -> f pred
      | `Match pred -> f pred
      | `NotMatch pred -> f ("not_" ^ pred))
      lc
end
  
let intreln_class_count ~classes (rel : Intreln.t) : (Experiment_t.predicate, int) acc_index =
  let c_count = new acc_index (+) 0 in
  rel |> Intreln.iter (fun seeds ->
    (classes#of_seeds seeds) |> List.iter (fun c ->
      c_count#add c 1));
  c_count

let concepts_of_example ~(store : pattern_store) ?timeout seeds examples = Common.prof "Cnnc.concepts_of_example" (fun () ->
  let open Cnn in
  let obs = Tarpit.blind_observer in
  let root = root ~obs ~store:(store :> Cnn.store)
    ~adjacent_edges:store#adjacent_edges
    ~relaxed_edges:(fun edge bounds -> EdgeSet.empty)
    ~seeds
    ~examples (*:(Intreln.of_intset (store#all_oids ~obs))*) in
  let strategy = new breadth_first_strategy in
  strategy#add root;
  discriminate ?timeout strategy;
  root#concepts)

(* probability of sampling at least [cx] positives among [x],
   when there are [c] positives among a total of [n] objects *)
let rec proba_sample_class =
  let ht = Hashtbl.create 1013 in
  fun cx x c n ->
    if cx > x || c > n then 0.
    else if cx = 0 then 1.
    else
      try Hashtbl.find ht (cx,x,c,n)
      with Not_found ->
	let p = (float c *. proba_sample_class (cx-1) (x-1) (c-1) (n-1) +. float (n-c) *. proba_sample_class cx (x-1) c (n-1)) /. float n in
	Hashtbl.add ht (cx,x,c,n) p;
	p

  
let list_best ?(factor = 1.) (l : ('a * float) list) : [`Best of 'a * float | `Ambiguous | `None] =
  (*  let l = Array.to_list (Array.mapi (fun i v -> (i,v)) ar) in*)
  let l = List.sort (fun (k1,v1) (k2,v2) -> Pervasives.compare v2 v1) l in
  match l with
  | (k1,v1)::(k2,v2)::_ ->
    assert (v1 >= v2);
    if v1 > 0. && v1 > factor *. v2
    then `Best (k1, v1 /. v2)
    else `Ambiguous
  | (k1,v1)::_ ->
     if v1 > 0.
     then `Best (k1, 1. /. 0.)
     else `None
  | _ -> `None

(*
let array_argmax (ar : 'a array) : int option =
  assert (Array.length ar > 0);
  let best_li, best_v = ref [0], ref ar.(0) in
  Array.iteri
    (fun i v ->
      if i=0 then ()
      else if v = !best_v then
	best_li := i::!best_li
      else if v > !best_v then
	begin
	  best_li := [i];
	  best_v := v
	end)
    ar;
  match !best_li with
  | [i] -> Some i
  | _ -> None

let predict_class_all ~no ~nc seeds_c concepts =
  let c_weight = Array.make nc 0. in
  Set.iter
    (fun concept ->
      let concept_weight =
	float no /. float concept.Cnn.distance in
      Intreln.iter
	(fun seeds ->
	  try
	    let c = Hashtbl.find seeds_c seeds in
	    c_weight.(c) <- c_weight.(c) +. concept_weight
	  with _ -> ())
	concept.Cnn.delta_extension)
    concepts;
  array_argmax c_weight, []
*)
    
let predict_class_premises ~options ~classes ~nb_examples ~c_count concepts = Common.prof "Cnnc.predic_class_premises" (fun () ->
  let c_weight = new acc_index (+.) 0. in
  let c_concepts = new acc_index (@) [] in
  let not_c_weight = new acc_index (+.) 0. in
  let not_c_concepts = new acc_index (@) [] in
  let ratio part all = float part /. float all in
  let factor pos_sample sample pos all = -. log (proba_sample_class pos_sample sample pos all) in
  let process c concept pos_sample sample pos all =
    let conf = ratio pos_sample sample in
    if conf >= options.min_conf
    then begin
	let fact = factor pos_sample sample pos all in
	if fact >= 3.
	then begin
	    let delta_weight = conf *. fact in
	    c_weight#add c delta_weight;
	    c_concepts#add c [(delta_weight,conf,concept)]
	  end
      end;
    let not_conf = ratio (sample - pos_sample) sample in
    if not_conf >= options.min_conf
    then begin
	let not_fact = factor (sample - pos_sample) sample (all - pos) all in
	if not_fact >= 3.
	then begin
	    let delta_weight = not_conf *. not_fact in
	    not_c_weight#add c delta_weight;
	    not_c_concepts#add c [(delta_weight,not_conf,concept)]
	  end
      end in
  Set.iter
    (fun concept ->
      if
      true
      (*concept.Cnn.distance >= 2*)
      (*not (Intreln.is_empty concept.Cnn.delta_extension)*)
      (*Intreln.cardinal concept.Cnn.extension = Intreln.cardinal concept.Cnn.delta_extension*)
      then begin
	(*let concept_count = Intreln.cardinal concept.Cnn.extension in*)
	let concept_count = Intreln.cardinal concept.Cnn.extension in
	let c_ext_count = intreln_class_count ~classes concept.Cnn.extension in
	let c_delta_count = intreln_class_count ~classes concept.Cnn.delta_extension in
	classes#iter
	  (fun c -> process c concept (c_ext_count#get c) concept_count (c_count#get c) nb_examples)
      end)
    concepts;
  let res = ref [] in
  classes#iter
    (fun c ->
     let c_w = c_weight#get c in
     let not_c_w = not_c_weight#get c in
     if c_w > 0. && c_w /. not_c_w >= options.min_factor
     then res := (c_w,c)::!res (* (c, 0., List.sort Pervasives.compare (c_concepts#get c))::res *)
     else ());
  List.map snd (List.rev (List.sort Pervasives.compare !res)))
(*
  (*print_string "\nscores: "; Array.iter (fun w -> Printf.printf "%.1f  " w) c_weight; print_newline ();*)
  let c_weight_list = c_weight#as_list in
  match list_best ~factor:options.min_factor c_weight_list with
  | `Best (c, ratio_with_second) -> `Best (c, ratio_with_second, List.sort Pervasives.compare (c_concepts#get c))
  | `Ambiguous -> `Ambiguous
  | `None -> `None)
 *)
   
let generate_seeds program_env pred vars =
  program_env#open_scope;
  Program.(run_pattern program_env
	     (Atom (pred, List.map (fun x -> AAtom (Var x)) vars)));
  let seeds = List.map (fun x -> program_env#get_var x) vars in
  program_env#close_scope;
  seeds
  
let rec fold_seq ~program_env ~arity (f : 'a -> string -> Cnn.oid list -> 'a) (init : 'a) (seq : Experiment_t.seq) : 'a =
  let get_vars prefix =
    List.map
      (fun i -> prefix ^ (if arity=1 then "" else "_" ^ string_of_int (i+1)))
      (Common.list_n arity) in
  match seq with
  | `Enum lpred ->
    List.fold_left
      (fun res pred ->
	let var_prefix = pred in
	let vars = get_vars var_prefix in
	let seeds = generate_seeds program_env pred vars in
	f res var_prefix seeds)
      init lpred
  | `Gen (n,pred) ->
    Common.fold_for
      (fun i res ->
	let var_prefix = pred ^ "_" ^ string_of_int i in
	let vars = get_vars var_prefix in
	let seeds = generate_seeds program_env pred vars in
	f res var_prefix seeds)
      1 n init

let iter_seq ~program_env ~arity (f : string -> Cnn.oid list -> unit) seq =
  fold_seq ~program_env ~arity (fun () name seeds -> f name seeds) () seq
let list_of_seq ~program_env ~arity seq =
  fold_seq ~program_env ~arity (fun l name seeds -> (name,seeds)::l) [] seq
let list_intreln_of_seq ~program_env ~arity seq =
  fold_seq ~program_env ~arity (fun (l,r) name seeds -> (name,seeds)::l, Intreln.add seeds r) ([],Intreln.empty arity) seq

class predictions ~store ~classes =
  let exp_rate = 0.9 in
object
  val mutable i = 0
(*  val mutable nb = 0
  val mutable nb_correct = 0
  val mutable expavg_correct = 0.0 *)
  val mutable sum_precision = 0.0
  val mutable sum_recall = 0.0

  method run_prediction ~options ~(examples : Intreln.t) ~(c_count : (predicate,int) acc_index) (name : string) (seeds : Cnn.oid list) = (* name of seeds *)
    i <- i+1;
    let lc = classes#of_seeds seeds in
    Printf.printf "%d.\to=%s\t" i name;
    let concepts = concepts_of_example ~store ~timeout:options.cnn_timeout seeds examples in
    let pred_lc = predict_class_premises ~options ~classes ~nb_examples:(Intreln.cardinal examples) ~c_count concepts in
    let delta_precision =
      let a, b = List.fold_left (fun (a,b) pred_c -> if List.mem pred_c lc then (a+1,b+1) else (a,b+1)) (0,0) pred_lc in
      if b=0 then 1. else float a /. float b in
    let delta_recall =
      let a, b = List.fold_left (fun (a,b) c -> if List.mem c pred_lc then (a+1,b+1) else (a,b+1)) (0,0) lc in
      if b=0 then 1. else float a /. float b in
    let correct = delta_precision = 1. && delta_recall = 1. in
    sum_precision <- sum_precision +. delta_precision;
    sum_recall <- sum_recall +. delta_recall;
    Printf.printf "%.3f\t%.3f\tlc=%s\t%s\n"
      (if i=0 then 1. else sum_precision /. float i)
      (if i=0 then 1. else sum_recall /. float i)
      (String.concat "," lc)
      (if correct then "YES" else ("no(" ^ String.concat "," pred_lc ^ ")"));
(*
    let pred_c, correct, ratio_with_second, c_concepts =
      match predict_class_premises ~options ~classes ~nb_examples:(Intreln.cardinal examples) ~c_count concepts with
      | `None ->
	 "?", false, 0., []
      | `Ambiguous ->
	 "*", false, 0., []
      | `Best (pred_c, ratio_with_second, c_concepts) ->
	let correct = (List.mem pred_c lc) in
	nb <- nb + 1;
	nb_correct <- nb_correct + (if correct then 1 else 0);
	expavg_correct <- exp_rate *. expavg_correct +. (1. -. exp_rate) *. (if correct then 1. else 0.);
	pred_c, correct, ratio_with_second, c_concepts in
    Printf.printf "%s\t%.2f\t%.3f\t%.3f\t%.3f\n"
      (if correct then "yes" else ("no(" ^ pred_c ^ ")"))
      ratio_with_second
      (float nb /. float i) (* decision rate *)
      (float nb_correct /. float nb) (* cumulated accuracy rate *)
      expavg_correct; (* exp. avg. accuracy rate *)
    if false (*show_concepts correct*) then begin
	(*print_string "data: "; Notation.Prolog.output_pattern stdout pattern; print_newline ();*)
      List.iter
	(fun (weight,conf,concept) ->
	  Printf.printf "\t\t\t%.1f/ %.2f/ %d/" weight conf concept.Cnn.ext_distance;
	  Cnn.print_intreln store ~max_rank:10 concept.Cnn.delta_extension;
	  print_string "/";
	  Cnn.print_edge_list (store :> Cnn.store) concept.Cnn.intension;
	  print_newline ())
	c_concepts
    end;
 *)
    flush stdout;
    
end

let run_experiment (exp : exp) : unit =
  let options = exp.options in
  let program =
    let ch_program = open_in exp.program in
    let prog = Program.parse_file ch_program in
    close_in ch_program;
    prog in
  let program_env = new Program.env in
  Program.run_program program_env program;
  let arity = exp.arity in
  let classes = new classes ~program_env ~arity exp.classes in
  let store = new pattern_store ~excluded_predicates:(classes#predicates) program_env#pattern in
  let predictions = new predictions ~store ~classes in
  match exp.task with
  | `Batch (training_seq,test_seq) ->
    let _, examples = list_intreln_of_seq ~program_env ~arity training_seq in
    let c_count = intreln_class_count ~classes examples in
    let test_seeds_list = list_of_seq ~program_env ~arity test_seq in
    store#pattern_changed;
    test_seeds_list |> List.iter
	(fun (name,seeds) ->
	  predictions#run_prediction ~options ~examples ~c_count name seeds)
  | `Cross seq_list ->
    let set_list = List.map (list_intreln_of_seq ~program_env ~arity) seq_list in
    store#pattern_changed;
    let split_list = (* list of (training,test) splits *)
      let rec aux = function
	| [] -> []
	| (set_l,set_r)::l ->
	  let split_l = aux l in
	  ((if l=[] then Intreln.empty arity else Intreln.union_r (List.map snd l)), set_l)
	  :: List.map
	    (fun (training,test) -> (Intreln.union set_r training, test))
	    split_l
      in
      aux set_list in
    split_list |> List.iter
	(fun (training,test) ->
	  let c_count = intreln_class_count ~classes training in
	  test |> List.iter
	      (fun (name,seeds) ->
		predictions#run_prediction ~options ~examples:training ~c_count name seeds))
  | `LeaveOneOut seq ->
    let l, r = list_intreln_of_seq ~program_env ~arity seq in
    store#pattern_changed;
    l |> List.iter
	(fun (name,seeds) ->
	  let examples = Intreln.remove seeds r in
	  let c_count = intreln_class_count ~classes examples in
	  predictions#run_prediction ~options ~examples ~c_count name seeds)
  | `Incr seq ->
    let examples = ref (Intreln.empty exp.arity) in
    let c_count = new acc_index (+) 0 in
    seq |> iter_seq ~program_env ~arity
	(fun name seeds ->
	  store#pattern_changed;
	  predictions#run_prediction ~options ~examples:(!examples) ~c_count name seeds;
	  examples := Intreln.add seeds !examples;
	  List.iter
	    (fun c -> c_count#add c 1)
	    (classes#of_seeds seeds))

let quit () =
(*  Printf.printf "Profiling...\n";
  let l =
    Hashtbl.fold
      (fun s elem res -> (elem.Common.prof_time, elem.Common.prof_nb, elem.Common.prof_mem,s)::res)
      Common.tbl_prof [] in
  let l = List.sort Pervasives.compare l in
  List.iter
    (fun (t,n,m,s) -> Printf.printf "%s: %d calls, %.1f seconds\n" s n t)
    l;*)
  exit 0

let _ =
  let exp_file = Sys.argv.(1) in
  let exp_ch = open_in exp_file in
  let exp = Experiment_j.read_exp (Yojson.Safe.init_lexer ()) (Lexing.from_channel exp_ch) in
  close_in exp_ch;
  Sys.set_signal Sys.sigint (Sys.Signal_handle (fun _ -> quit ()));
  run_experiment exp;
  quit ()
