open Patterns

let test_automorphisms () =
  let open Patterns in
  let p1 = new pattern in
  p1#add_edge "p" [Var 1; Var 2];
  p1#add_edge "p" [Var 1; Var 3];
  p1#add_edge "p" [Var 1; Var 4];
  p1#add_edge "q" [Var 2; Var 5];
  p1#add_edge "q" [Var 3; Var 6];
  p1#add_edge "q" [Var 4; Var 7];
  let p2 = new pattern in
  p2#add_edge "a" [Var 1; Var 2];
  p2#add_edge "b" [Var 2; Var 3];
  p2#add_edge "c" [Var 3; Var 4];
  p2#add_edge "a" [Var 4; Var 5];
  p2#add_edge "b" [Var 5; Var 6];
  p2#add_edge "c" [Var 6; Var 1];
  let p3 = new pattern in
  p3#add_edge "p" [Var 1; Var 2];
  p3#add_edge "p" [Var 1; Var 3];
  p3#add_edge "p" [Var 1; Var 4];
  p3#add_edge "p" [Var 1; Var 5];
  p3#add_edge "q" [Var 2; Var 3];
  p3#add_edge "q" [Var 3; Var 2];
  p3#add_edge "q" [Var 4; Var 5];
  p3#add_edge "q" [Var 5; Var 4];
  let p4 = new pattern in
  p4#add_edge "p" [Var 1; Var 2];
  p4#add_edge "p" [Var 1; Var 4];
  p4#add_edge "q" [Var 2; Var 3];
  p4#add_edge "q" [Var 4; Var 5];
  p4#add_edge "q" [Var 4; Var 6];
  let p5 = new pattern in
  p5#add_edge "p" [Var 1; Var 2];
  p5#add_edge "p" [Var 1; Var 3];
  p5#add_edge "q" [Var 2; Var 4];
  p5#add_edge "q" [Var 3; Var 5];
  p5#add_edge "r" [Var 4; Var 6];
  p5#add_edge "r" [Var 4; Var 7];
  p5#add_edge "r" [Var 5; Var 6];
  p5#add_edge "r" [Var 5; Var 7];
  p5#add_edge "s" [Var 6; Var 7];
  p5#add_edge "s" [Var 7; Var 6];
  let p6 = new pattern in
  p6#add_edge "a" [Var 0; Var 1];
  p6#add_edge "a" [Var 1; Var 2];
  p6#add_edge "a" [Var 2; Var 3];
  p6#add_edge "a" [Var 3; Var 0];
  let px = p6 in
  print_endline "automorphism\t\torbits\t\tretract";
  px#canonical_automorphisms
  |> snd
  |> List.iter
       (fun auto ->
	let p = Permutation.orbits auto in
	print_string (Permutation.to_string auto);
	print_string "\t\t";
	print_string p#to_string;
	print_string "\t\t";
	print_string
	  ( match px#symmetric_retract_from_automorphism auto with
	    | None -> "NONE"
	    | Some r -> Retract.to_string r );
	print_newline());
  print_endline "symmetric retract nodes";
  print_endline
    ( match px#symmetric_retract with
      | _, None -> "NONE"
      | _, Some sx -> String.concat " " (List.rev (Intset.map string_of_int sx)) );
  print_endline "fully retracted pattern";
  ()
  (*let _renaming, _repr = px#remove_duplicate_nodes in*)
  (*Notation.Prolog.output_pattern stdout px; print_newline ()*)
  (*print_endline "canonical repr";
  Notation.Prolog.output_edges stdout repr; print_newline ()*)

(*let test_core_vars () =
  let open Patterns in
  let p1 = new pattern in
  p1#add_edge "np" [Var 1; Var 2];
  p1#add_edge "np" [Var 1; Var 3];
  p1#add_edge "np" [Var 1; Var 4];
  p1#add_edge "np" [Var 1; Var 5];
  p1#add_edge "noun" [Var 2; Var 6];
  p1#add_edge "noun" [Var 3; Var 8];
  p1#add_edge "det" [Var 2; Var 7];
  p1#add_edge "det" [Var 4; Var 9];
  p1#add_edge "l" [Var 6; Val Val.Any];
  p1#add_edge "l" [Var 7; Val Val.Any];
  p1#add_edge "l" [Var 8; Val Val.Any];
  p1#add_edge "l" [Var 9; Val Val.Any];
  let px = p1 in
  let core, _ = core_vars px in
  List.iter
    (fun x ->
     let domains_x = List.map (fun x -> (x, Intset.singleton x)) (x::core) in
     let core_x, _ = core_vars ~init_domains:domains_x px in
     Printf.printf "core of %d: %s\n" x (Intset.fold (fun res x -> res ^ " " ^ string_of_int x) "" core))
    (List.sort Pervasives.compare px#var_list)
 *)
							  
let _ =
  Printexc.record_backtrace true;
  test_automorphisms ()
		     (*test_core_vars ()*)
