

open Patterns
open Generation

(* profiling *)
      
let prerr_profiling () =
  Printf.eprintf "// Profiling...\n";
  let l =
    Hashtbl.fold
      (fun s elem res -> (elem.Common.prof_time, elem.Common.prof_nb, elem.Common.prof_mem,s)::res)
      Common.tbl_prof [] in
  let l = List.sort Stdlib.compare l in
  List.iter
    (fun (t,n,m,s) -> Printf.eprintf "// %s: %d calls, %.1f seconds\n" s n t)
    l;
  flush stderr

(* main *)
    
let main () =
  let arg_mode = ref `Lin in
  let arg_only_cores = ref false in
  let arg_max_level = ref None in
  let arg_max_vars = ref None in
  let arg_output_format : [`None|`Txt|`Json|`Dot] ref = ref `None in
  let arg_dot_ctx = ref false in
  let arg_dot_sub = ref false in
  let arg_dot_norel = ref false in
  let arg_ext = ref false in
  let arg_injective_extensions = ref false in
  let arg_min_supp = ref 1 in
  let arg_supp = ref false in
  let arg_max_size = ref None in
  let arg_filename = ref "a" in
  let input = ref stdin in
  Arg.parse [
     "-quad", Arg.Unit (fun () -> arg_mode := `Quad), "use quadratic mode for generation";
     "-only-cores", Arg.Unit (fun () -> arg_only_cores := true), "only compute and display core patterns";
     "-l", Arg.Int (fun l -> arg_max_level := Some l), "maximum number of generation levels";
     "-n", Arg.Int (fun n -> arg_max_vars := Some n), "maximum number of variables in generated patterns";
     "-minsupp", Arg.Int (fun m -> arg_ext := true; arg_min_supp := m), "filter concepts by minimum support (default 1)";
     "-maxsize", Arg.Int (fun ms -> arg_max_size := Some ms), "filter concepts by maximum size (nb. nodes)";
     "-txt", Arg.Unit (fun () -> arg_output_format := `Txt), "show concepts as text in terminal";
     "-json", Arg.Unit (fun () -> arg_output_format := `Json), "show concepts as JSON in terminal";
     "-dot", Arg.Unit (fun () -> arg_output_format := `Dot), "show concepts as SVG";
     "-ctx", Arg.Set arg_dot_ctx, "include context graph in DOT output";
     "-sub", Arg.Unit (fun () -> arg_ext := true; arg_dot_sub := true), "include subsumption relationships between concepts in DOT output";
     "-norel", Arg.Set arg_dot_norel, "displays relational edges as attributes inside nodes (e.g., to better see subsumptions relations)";
     "-ext", Arg.Set arg_ext, "print extents for each node";
     "-injective", Arg.Unit (fun () -> arg_ext := true; arg_injective_extensions := true), "show only injective mappings in solutions";
     "-supp", Arg.Unit (fun () -> arg_ext := true; arg_supp := true), "print only concept support rather than object list";
    ]
    (fun filename ->
      arg_filename := filename;
      input := open_in filename)
    "./gfca [-quad] [-only-cores] [-l <int>] [-n <int>] [-minsupp <int>] [-maxsize <int>] [-txt | -json | -dot [-ctx] [-sub] [-norel]] [-ext] [-injective] [-supp] <filename>";

    (* parsing input graph *)
    let (g :pattern), (env : Program.env) = Program.run (Program.parse_file !input) in
    Printf.eprintf "Nb. nodes: %d\n" g#nb_vars;
    Printf.eprintf "Nb. edges: %d\n" g#nb_edges;
    (* computing patterns *)
    let queries =
      if !arg_max_level = Some 0
      then []
      else
	generate_concepts_by_core
	  ~mode:!arg_mode ~only_cores:!arg_only_cores
	  ?max_level:!arg_max_level ?max_vars:!arg_max_vars
	  ~min_supp:!arg_min_supp ?max_size:!arg_max_size
	  ~show_extent:!arg_ext ~injective:!arg_injective_extensions
	  g in
    let lcover =
      if !arg_dot_sub
      then Generation.upper_covers queries
      else [], [] in
    Printf.eprintf "Nb. core patterns: %d\n" (List.length queries);
    Printf.eprintf "Nb. pattern nodes: %d\n"
		   (List.fold_left
		      (fun nb c -> nb + c.Generation.pattern#nb_vars)
		      0 queries);
    Printf.eprintf "Nb. pattern edges: %d\n"
		   (List.fold_left
		      (fun nb c -> nb + c.Generation.pattern#nb_edges)
		      0 queries);
    if !arg_ext then
      Printf.eprintf "Nb. unary concepts: %d\n"
        (Generation.cbcs_nb_extents queries);
    Printf.eprintf "\n";
    prerr_profiling ();
    (* outputing patterns *)
    let txt_filename = Filename.chop_extension !arg_filename ^ ".concepts.txt" in
    let _ =
      let output = open_out txt_filename in
      Notation.Prolog.output_query_list ~supp_only:!arg_supp ~g output queries;
      close_out output in
    let json_filename = Filename.chop_extension !arg_filename ^ ".concepts.json" in
    let _ =
      let output = open_out json_filename in
      Notation.Json.output_query_list ~g output queries lcover;
      close_out output in
    let dot_filename = Filename.chop_extension !arg_filename ^ ".concepts.dot" in
    let svg_filename = Filename.chop_extension !arg_filename ^ ".concepts.svg" in
    let _ =
      let queries =
	if !arg_dot_ctx
	then
	  let query_context = { core_id = 0; core_vars = g#var_list; pattern = g; intents = []; inverted_extent = [] } in
	  query_context::queries
	else
	  queries in
      let output = open_out dot_filename in
      Notation.Dot.output_query_list env ~no_rel:!arg_dot_norel ~supp_only:!arg_supp ~g output queries lcover;
      close_out output;
      Sys.command ("dot -Tsvg " ^ dot_filename ^ " > " ^ svg_filename) in
    ( match !arg_output_format with
    | `None -> ()
    | `Txt -> ignore (Sys.command ("cat " ^ txt_filename))
    | `Json -> ignore (Sys.command ("cat " ^ json_filename))
    | `Dot -> ignore (Sys.command ("xdg-open " ^ svg_filename))
    )

let _ =
  Printexc.record_backtrace true;
  main ()
