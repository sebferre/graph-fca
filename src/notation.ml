
open Patterns
open Generation

let rec letters_of_int j =
  if j < 0
  then ""
  else
    let letters = String.make 1 (Char.chr (Char.code 'a' + j mod 26)) in
    let j_26 = j / 26 in
    if j_26 = 0
    then letters
    else letters_of_int j_26 ^ letters

class name_of_var (l_cbc : concepts_by_core list) =
object (self)
  val core_cpt : (int, int ref) Hashtbl.t = Hashtbl.create 100
  val ht : (int*int, int) Hashtbl.t = Hashtbl.create 100
  (* mapping (core_id,node_id) to node_rank in core_id *)
  method get_number (i : core_id) (v : var) : int =
    try Hashtbl.find ht (i,v)
    with Not_found ->
      let cpt =
	try Hashtbl.find core_cpt i
	with Not_found ->
	  let cpt = ref (-1) in
	  Hashtbl.add core_cpt i cpt;
	  cpt in
      incr cpt;
      Hashtbl.add ht (i,v) !cpt;
      !cpt
  method get (i : core_id) (v : var) : string =
    letters_of_int (self#get_number i v)

  initializer (* to give names to vars in a nice and constant order *)
    List.iter
      (fun cbc ->
       let _ = List.map (self#get cbc.core_id) (List.rev cbc.core_vars) in
       let _ = List.map (self#get cbc.core_id) cbc.pattern#var_list in
       ())
      l_cbc
end

let get_color, get_color' =
  let max_split, splits = 16, [| 0; 10; 5; 15; 2; 12; 7; 4; 14; 9; 1; 11; 6; 3; 13; 8 |] in (* avoiding median values for sharp colors *)
  let nbsplits = Array.length splits in
  let base = 127 in
  let levels = [| 95; 159; 191 |] in (* should not include base so as to avoid doublons *)
  let nblevels = Array.length levels in
  let chunk = (255 - base) / max_split in
  let nbcolors = 3 * nblevels * nbsplits in
  let t = Array.make nbcolors "white" in
  let t' = Array.make nbcolors "lightgrey" in
  let lighten x = x + (256 - x) * 4 / 5 in
  for i = 0 to nbsplits - 1 do (* for all split between 2 colors *)
    let x1 = splits.(i) in
    let x2 = max_split - x1 in
    for l = 0 to nblevels - 1 do (* for all levels *)
      let x, y, z = base + x1*chunk, base + x2*chunk, levels.(l) in
      for j = 0 to 2 do (* for every combination of 2 colors *)
	let pos = 3*i + 3*nblevels*l + j in
	let r, g, b =
	  match j with
	  | 0 -> z, x, y (* green-blue *)
	  | 1 -> y, z, x (* blue-red *)
	  | _ -> x, y, z (* red-green *) in
	t.(pos) <- Printf.sprintf "#%02x%02x%02x" r g b;
	let r', g', b' = lighten r, lighten g, lighten b in
	t'.(pos) <- Printf.sprintf "#%02x%02x%02x" r' g' b'
      done
    done
  done;
  (fun i -> if i > 0 then t.((i-1) mod nbcolors) else "white"),
  (fun i -> if i > 0 then t'.((i-1) mod nbcolors) else "lightgrey")

  
module Prolog =
struct
  (* printing *)

  let rec output_query_list ~supp_only ~g out lq =
    let name_of_var = new name_of_var lq in
    List.iter
      (fun q ->
       output_query ~supp_only ~g ~name_of_var out q)
      lq
  and output_query ~supp_only ~g ~name_of_var out cbc =
    let core_vars = List.rev cbc.core_vars in
    output_generator ~name_of_var out cbc.core_id core_vars;
(*    if not no_projection && lgen <> [] then begin
      ignore (List.fold_left
		(fun j gen ->
		  output_string out ", ";
		  output_generator out i j gen;
		  j+1)
		1 lgen)
      end;*)
    output_string out " :- ";
    output_pattern ~name_of_var out cbc.core_id cbc.pattern;
    output_string out ".\n";
    output_inverted_extent ~supp_only ~name_of_var out cbc.core_id cbc.pattern g cbc.inverted_extent;
    (*match extent_opt with None -> () | Some extent -> output_extent out p g extent *)
    output_string out "\n"
  and output_generator ~name_of_var out core_id lx =
    output_edge ~name_of_var out
		core_id ("Q" ^ string_of_int core_id, List.map (fun x -> Var x) lx)
  and output_pattern ~name_of_var out core_id p =
    let var_list =
      List.sort Stdlib.compare
		(List.map (fun x -> (name_of_var#get_number core_id x, x)) p#var_list) in
    ignore (List.fold_left
	      (fun first (_,x) ->
		let edges = p#var_edges x in
		List.fold_left
		  (fun first (a,(ll,rr)) ->
		    if ll=[] (* otherwise redundant *)
		    then begin
		      if not first then output_string out ", ";
		      output_edge ~name_of_var out core_id (a, Var x :: rr);
		      false end
		    else first)
		  first edges)
	      true var_list)
  and output_edges ~name_of_var out core_id = function
    | [] -> output_string out "true"
    | [edge] -> output_edge ~name_of_var out core_id edge
    | edge::edges -> output_edge ~name_of_var out core_id edge; output_string out ", "; output_edges ~name_of_var out core_id edges
  and output_edge ~name_of_var out core_id (a,args) =
    output_attr ~name_of_var out a;
    output_args ~name_of_var out core_id args
  and output_attr ~name_of_var out a =
    output_string out a
  and output_args ~name_of_var out core_id args =
    List.iter (fun arg -> output_string out " "; output_arg ~name_of_var out core_id arg) args
(*  and output_args out = function
    | [] -> output_string out "()"
    | arg::args ->
      output_string out "(";
      output_arg out arg;
      List.iter (fun arg -> output_string out ","; output_arg out arg) args;
    output_string out ")"*)
  and output_arg ~name_of_var out core_id = function
    | Var x -> output_string out ("X" ^ name_of_var#get core_id x)
    | Val v -> output_string out (string_of_val v)
  and output_inverted_extent ~supp_only ~name_of_var out core_id p g inv_ext =
    let get_label var_prefix pat o =
      match pat#get_label o with
      | Some s -> s ^ "_" ^ string_of_int o
      | None -> var_prefix ^ string_of_int o in
    let inv_ext =
      List.sort Stdlib.compare
		(List.map
		   (fun (x,ext) -> (name_of_var#get_number core_id x, x, ext))
		   inv_ext) in
    List.iter
      (fun (_,x,ext) ->
	output_string out ("% X" ^ name_of_var#get core_id x ^ ":");
	if supp_only
	then output_string out (" " ^ string_of_int (Intset.cardinal ext))
	else Intset.iter (fun o -> output_string out (" " ^ get_label "_" g o)) ext;
	output_string out "\n")
      inv_ext
  and output_extent ~name_of_var out core_id p g (vars,rel) =
    let n = List.length vars in
    let cell_size = 14 in
    output_string out ("% " ^ String.make (cell_size*n) '-' ^ "\n");
    output_sol ~name_of_var out cell_size "X" core_id p vars;
    output_string out ("% " ^ String.make (cell_size*n) '=' ^ "\n");
    Intreln.iter (fun sol -> output_sol ~name_of_var out cell_size "_" core_id g sol) rel;
    output_string out ("% " ^ String.make (cell_size*n) '-' ^ "\n")
  and output_sol ~name_of_var out cell_size var_prefix core_id p vars =
    output_string out "% ";
    List.iter
      (fun x ->
        let label =
	  if var_prefix = "X"
	  then "X" ^ name_of_var#get core_id x
	  else
	    match p#get_label x with
	    | Some s -> s ^ "_" ^ string_of_int x
	    | None -> var_prefix ^ string_of_int x in
	let label =
	  let n = String.length label in
	  if n >= cell_size
	  then String.sub label 0 cell_size
	  else label ^ String.make (cell_size-n) ' ' in
	output_string out label; output_string out " ")
      vars;
    output_string out "\n"

end (* Prolog *)


module Json =
  struct

    let output_json_bool (b : bool) out indent : unit =
      output_string out (string_of_bool b)
    let output_json_int (i : int) out indent: unit =
      output_string out (string_of_int i)
    let output_json_string (s : string) out indent : unit =
      output_string out ("\"" ^ String.escaped s ^ "\"")

    type indent = string
    let init_indent : indent = "\n"
    let increase_indent (indent : indent) : indent = indent ^ "   "
		    
    let output_array ?(newline = false) (output_elt : 'a -> out_channel -> indent -> unit) (le : 'a list) out indent : unit =
      let elt_indent = increase_indent indent in
      output_string out "[";
      ignore (List.fold_left
		(fun first e ->
		 if not first then output_string out ", ";
		 if newline then output_string out elt_indent;
		 output_elt e out elt_indent;
		 false)
		true le);
      output_string out "]"

    let output_record ?(newline = false) (fields : (string * (out_channel -> indent -> unit) option) list) out indent : unit =
      let field_indent = increase_indent indent in
      output_string out "{";
      ignore (List.fold_left
		(fun first (name,output_val_opt) ->
		 match output_val_opt with
		 | None -> first
		 | Some output_val ->
		    if not first then output_string out ", ";
		    if newline then output_string out field_indent;
		    output_json_string name out indent;
		    output_string out ": ";
		    output_val out field_indent;
		    false)
		true fields);
      output_string out "}"

    let output_object ~g o out indent =
      match g#get_label o with
      | Some s -> output_json_string s out indent
      | None -> output_json_string ("_" ^ string_of_int o) out indent

    let output_concept ~name_of_var (core_id,x) out indent =
      output_record
	["Q" ^ string_of_int core_id,
	 Some (output_json_string (name_of_var#get core_id x))]
	out indent
	
    let output_cbc_node ~g ~name_of_var ~cbc ~lcover:(concepts,upper_covers) x out indent =
      let core_id = cbc.core_id in
      let intent_nodes =
	try
	  List.assoc x cbc.intents
	  |> List.sort
	       (fun y1 y2 -> Stdlib.compare
			       (name_of_var#get_number core_id y1)
			       (name_of_var#get_number core_id y2))
	with _ -> [] in
      let extent = try Intset.elements (List.assoc x cbc.inverted_extent) with _ -> [] in
      let upper_cover =
	List.sort
	  (fun (c1,x1) (c2,x2) -> Stdlib.compare
				    (c1, name_of_var#get_number c1 x1)
				    (c2, name_of_var#get_number c2 x2))
	  (List.fold_left
	     (fun res (cluster,cover) ->
	      if List.mem (core_id,x) cluster
	      then List.map List.hd cover @ res
	      else res)
	     [] upper_covers) in
      output_record ~newline:true
	["inCore", Some (output_json_bool (List.mem x cbc.core_vars));
	 "intentNodes", Some (output_array
				(fun x -> output_json_string
					    (name_of_var#get core_id x))
				intent_nodes);
	 "extent", (if extent=[]
		   then None
		   else Some (output_array (output_object ~g) extent));
	 "upperCover", (if upper_covers = []
		       then None
		       else Some (output_array (output_concept ~name_of_var) upper_cover))]
	out indent

    let output_arg ~name_of_var ~cbc arg out indent =
      match arg with
      | Var y -> output_json_string (name_of_var#get cbc.core_id y) out indent
      | Val v -> output_record ["value", Some (output_json_string (string_of_val v))] out indent
	
    let output_cbc_edge ~g ~name_of_var ~cbc (a,args) out indent =
      output_record
	[a, Some (output_array (output_arg ~name_of_var ~cbc) args)]
(*	["pred", output_json_string a;
	 "args", output_array (output_arg ~name_of_var) args] *)
	out indent
      
    let output_cbc ~name_of_var ~g cbc lcover out indent =
      let core_id = cbc.core_id in
      let nodes =
	List.sort
	  (fun x1 x2 -> Stdlib.compare (name_of_var#get_number core_id x1)
					   (name_of_var#get_number core_id x2))
	  cbc.pattern#var_list in
      let indexed_edges =
	List.map
	  (fun (a,args as edge) ->
	   let key =
	     List.map
	       (function
		 | Var x -> name_of_var#get_number core_id x
		 | Val _ -> -1)
	       args,
	     a in
	   (key,edge))
	  cbc.pattern#edges in
      let edges =
	List.map
	  snd
	  (List.sort Stdlib.compare indexed_edges) in
      output_record
	~newline:true
	[(*"coreId", output_json_int cbc.core_id;*)
	  "nodes",
	  Some (output_record
		  ~newline:true
		  (List.map
		     (fun x -> name_of_var#get cbc.core_id x,
			       Some (output_cbc_node ~g ~name_of_var ~cbc ~lcover x))
		     nodes));
(*	 output_array ~newline:true
		      (output_cbc_node ~g ~name_of_var ~cbc)
		      cbc.pattern#var_list; *)
	  "edges",
	  Some (output_array
		  ~newline:true
		  (output_cbc_edge ~g ~name_of_var ~cbc)
		  edges)]
	out indent

    (*let output_cover ((core_id,x), cover) out =
      output_record
	["coreId", output_json_int core_id;
	 "name", output_json_string *)
	
    let output_query_list ~g out lq lcover =
      let name_of_var = new name_of_var lq in
      output_record
	~newline:true
	(List.map
	   (fun cbc -> "Q" ^ string_of_int cbc.core_id,
		       Some (output_cbc ~name_of_var ~g cbc lcover))
	   lq)
	(*"covers", output_array (output_cover ) lcover]*)
	out init_indent;
      output_string out "\n"

  end
  
module Dot =
struct
  let shape_node = "record"
  let shape_edge = "oval"
  let shape_proj = "diamond"
  
  let output_newline out () = output_string out "\n"
  let output_int out n = output_string out (string_of_int n)
  let output_var out i x = output_string out "x"; output_int out i; output_string out "_"; output_int out x

  let output_attrs out l =
    output_string out " [";
    List.iter (fun (a,v) -> output_string out (a ^ "=\"" ^ String.escaped v ^ "\"")) l;
    output_string out "]"

  let output_edge_group env out val_cpt edge_cpt core_id (la,args,is_stree,is_rel) in_core =
    let fillcolor = if in_core then get_color core_id else get_color' core_id (*"lightgrey"*) in
    let ids =
      List.map
	(function
	| Var x ->"x" ^ string_of_int core_id ^ "_" ^ string_of_int x
	| Val v ->
	  let id = incr val_cpt; ("v" ^ string_of_int core_id ^ "_" ^ string_of_int !val_cpt) in
	  let label = string_of_val v in
	  output_string out id; output_attrs out [("label",label); ("shape",shape_node); ("style","filled"); ("fillcolor",fillcolor)];
	  id)
	args in
    let label_la = String.concat "\n" la in
    match ids with
    (*    | [id] -> output_string out id; output_attrs out [("xlabel",a)]; output_newline out ()*)
    | [id1;id2] ->
       output_string out id1;
       output_string out " -> ";
       output_string out id2;
       if is_rel then output_string out ":n";
       output_attrs out [("label",label_la); ("penwidth","2")]; output_newline out ()
    | _ ->
      let j = incr edge_cpt; !edge_cpt in
      let edge_id = "e" ^ string_of_int core_id ^ "_" ^ string_of_int j in
      output_string out edge_id; output_attrs out [("label",label_la); ("shape",shape_edge); ("style","filled"); ("fillcolor",fillcolor)]; output_newline out ();
      ignore (List.fold_left
		(fun k id ->
		  if k=1 then (
		    output_string out id;
		    output_string out " -> ";
		    output_string out edge_id;
		    if is_rel then output_string out ":n"
		  ) else (
		    output_string out edge_id;
		    if is_stree && k=2 then output_string out ":w";
		    if is_stree && k=3 then output_string out ":s";
		    if is_stree && k=4 then output_string out ":e";
		    output_string out " -> ";
		    output_string out id;
		    if is_rel && k=2 then output_string out ":n";
		  );
		  output_attrs out [("label",string_of_int k); ("penwidth","2")];
		  output_newline out ();
		  k+1)
		1 ids)

  let output_pattern env ~no_rel ~supp_only ~g ~name_of_var out core_id core p intents inv_ext =
    let edge_cpt = ref 0 in
    let val_cpt = ref 0 in
    List.iter
      (fun x ->
       let intent_opt =
         try Some (List.assoc x intents)
         with Not_found -> None in
       let node_label =
         if core_id = 0 (* graph context *)
         then
	   match p#get_label x with
	   | Some s -> s
	   | None -> "_" ^ string_of_int x
         else
	   let node_name = "Q" ^ string_of_int core_id ^ name_of_var#get core_id x in
           let node_name =
             match intent_opt with
             | Some intent ->
                let proper_intent = List.filter (fun y -> not (List.mem y (x::core))) intent in
	        if proper_intent = []
	        then node_name
	        else node_name ^ " (" ^ String.concat " " (List.map (name_of_var#get core_id) proper_intent) ^ ")"
             | None -> (* missing intent *)
                node_name ^ " (??)" in
           node_name in
       let int_label = (* only unary predicates applying to this node *)
	 let arg_in_intent =
	   match intent_opt with
	   | None -> (fun arg -> true) (* TODO: should not happen *)
	   | Some intent ->
	      ( function
	      | Var y -> List.mem y intent
	      | Val _ -> true ) in
	 let string_of_arg = function
	   | Var y -> name_of_var#get core_id y
	   | Val v -> string_of_val v in
	 let la, lp =
	   List.fold_left
	     (fun (la,lp) (a,args_ctx) ->
	      match args_ctx with
	      | ([],[]) -> a::la, lp
	      | (ll,rr) ->
		 if no_rel
		    && List.for_all arg_in_intent ll
		    && List.for_all arg_in_intent rr
		       (* only edges in concept intent *)
		 then
		   let ls =
		     a
		     :: List.rev_map string_of_arg ll
		     @ "_"
		       :: List.map string_of_arg rr in
		   la, String.concat " " ls :: lp
		 else la, lp)
	     ([],[]) (p#var_edges x) in
	 String.concat "\n" (la @ lp) in
       let ext_label =
         if core_id = 0 (* graph context *)
         then ""
         else
	   try
	     let lo = List.assoc x inv_ext in
	     if supp_only
	     then "(" ^ string_of_int (Intset.cardinal lo) ^ ")"
	     else
	       String.concat
	         "\n"
	         (List.rev
                    (Intset.fold
		       (fun ext_labels o ->
		         let label =
		           match g#get_label o with
		           | Some s -> s
		           | None -> "_" ^ string_of_int o in
		         label::ext_labels)
		       [] lo))
	   with _ -> "??" in
       let label =
	 match int_label, ext_label with
	 | "", "" -> node_label
	 | _, "" -> "{" ^ node_label ^ "|" ^ int_label ^ "}"
	 (*| "", _ -> "{" ^ node_label ^ "|" ^ ext_label ^ "}"*)
	 | _ -> "{" ^ node_label ^ "|" ^ int_label ^ "|" ^ ext_label ^ "}"  in
       let fillcolor = if List.mem x core then get_color core_id (* white *) else get_color' core_id (*"lightgrey"*) in
       output_var out core_id x; output_attrs out [("label",label); ("shape",shape_node); ("style","filled"); ("fillcolor",fillcolor)]; output_newline out ())
      p#var_list;
    let grouped_edges =
      let h = Hashtbl.create 101 in
      List.iter
	(fun (a,args) ->
	 let is_stree = env#stree_predicates#mem a in
	 let is_rel = not is_stree && env#rel_predicates#mem a in
	 let key = (args,is_stree,is_rel) in
	 let la =
	   try Hashtbl.find h key
	   with Not_found -> [] in
	 Hashtbl.replace h key (a::la))
	p#edges;
      Hashtbl.fold
	(fun (args,is_stree,is_rel) la res -> (la,args,is_stree,is_rel)::res)
	h [] in
    if not no_rel then
      List.iter
	(fun (_,args,_,_ as edge_group) ->
	 match args with
	 | [_] -> () (* unary edges are included in node labels (see above) *)
	 | _ ->
	    let in_core =
	      List.for_all
		(function
		  | Var x -> List.mem x core
		  | Val _ -> true)
		args in
	    output_edge_group env out val_cpt edge_cpt core_id edge_group in_core)
	grouped_edges

  let output_query env ~no_rel ~supp_only ~name_of_var ~g out cbc (*core,p,intents,inv_ext*) =
    (*output_string out "subgraph cluster_";
    output_int out cbc.core_id;
    output_string out " {\n";*)
    output_string out "{\n";
    (*if not no_projection then
      ignore (List.fold_left
		(fun j gen ->
		  output_edge_node out "p" i j;
		  output_attrs out [("label", "Q" ^ string_of_int i ^ letters_of_int j); ("shape",shape_proj)];
		  output_newline out ();
		  List.iter
		    (fun x ->
		      output_edge_node out "p" i j;
		      output_string out " -> ";
		      output_var out i x;
		      output_newline out ())
		    gen;
		  j+1)
      0 lgen);*)
    output_pattern env ~no_rel ~supp_only ~g ~name_of_var out cbc.core_id cbc.core_vars cbc.pattern cbc.intents cbc.inverted_extent;
    output_string out "}\n";
    flush out

  let output_covers out (concepts, upper_covers) =
    let output_cluster out nodes =
      output_string out "cluster";
      List.iter
        (fun (core_id,x) ->
          output_string out "_";
          output_var out core_id x)
        nodes
    in
    (* declaring sets of equivalent concepts as DOT clusters *)
    List.iter
      (function
       | [] -> assert false
       | [_] -> ()
       | nodes -> (* declare a cluster of equivalent nodes *)
          output_string out "subgraph ";
          output_cluster out nodes;
          output_string out " {\nstyle=\"rounded,dashed\";\n";
          List.iter
            (fun (core_id,x) ->
              output_var out core_id x;
              output_newline out ())
            nodes;
          output_string out "}\n")
      concepts;
    (* declaring subsumption links *)
    List.iter
      (fun (concept, cover) ->
        let n = List.length concept in
        let core_id, x = List.hd concept in
        List.iter
	  (fun concept' ->
            let n' = List.length concept' in
            let core_id', x' = List.hd concept' in
	    output_var out core_id' x';
	    output_string out " -> ";
	    output_var out core_id x;
            output_string out " [style=\"dashed\"";
            if n' > 1 then (
              output_string out "; ltail=";
              output_cluster out concept');
            if n > 1 then (
              output_string out "; lhead=";
              output_cluster out concept);
            output_string out "]\n")
	 cover)
      upper_covers
	  
  let output_query_list env ~no_rel ~supp_only ~g out lq lcover =
    let name_of_var = new name_of_var lq in
    output_string out "digraph patterns {\n";
    output_string out "ranksep=0.8;\n";
    output_string out "nodesep=0.2;\n";
    output_string out "graph [compound=true]\n";
(*    output_string out "node [fontsize=24];";
    output_string out "edge [fontsize=24];"; *)
    List.iter (fun q -> output_query env ~no_rel ~supp_only ~name_of_var ~g out q) lq;
    output_covers out lcover;
    output_string out "}\n"
    
end
