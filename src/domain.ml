
(* interfaces *)

module type CORE =
  sig
    type t
    val parse : (unit, t, Matcher.cursor) Dcg.parse
    val print : (t, Printer.cursor, unit) Ipp.print
  end
  
module type ORDER =
  sig
    include CORE
    val compare : t -> t -> int
  end  

module type T =
  sig
    include CORE
    val similarity : t -> t -> t (* least general generalization *)
    val subsumed : t -> t -> bool (* is more general than *)
    val bottom : t
  end
  
let print_eof print = ipp [ v -> print of v; EOF ]

(* primitive value types, not domains *)

module String : CORE with type t = string =
  struct
    type t = string
                           
    let parse = dcg
      [ "\""; s = match "[^\"]*" as "string"; "\"" -> s
      | s = match "[0-9]+" as "int" -> s ] (* kept for bwd compatibility *)

    let print = ipp
      [ s -> "\""; '(String.escaped s); "\"" ]
  end

module Int : ORDER with type t = int =
  struct
    type t = int
    let compare x y =
      if x = y then 0
      else if x < y then (-1)
      else (* x > y *) 1
    let parse = dcg [ s = match "[0-9]+" as "int" -> int_of_string s ]
    let print = ipp [ i -> '(string_of_int i) ]
  end

  
(* domain functors *)
                    
module Nominal(X : CORE) : T =
  struct
    type t = Any | X of X.t

    let similarity v1 v2 =
      match v1, v2 with
      | X x1, X x2 when x1=x2 -> v1
      | _ -> Any

    let subsumed v1 v2 =
      match v1, v2 with
      | Any, _ -> true
      | _, Any -> false
      | X x1, X x2 -> x1 = x2

    let bottom = Any

    let parse = dcg
      [ "*" -> Any
      | x = X.parse -> X x ]

    let print = ipp
      [ Any -> "*"
      | X x -> X.print of x ]
  end
                        
module Interval
         (X : ORDER)
         (Param : sig val normalize : X.t * X.t -> (X.t * X.t) option end)
       : T =
  struct
    type t = Any | Interv of X.t * X.t

    let normalize a b =
      match Param.normalize (a,b) with
      | Some (a,b) -> Interv (a,b)
      | None -> Any
                           
    let similarity v1 v2 =
      match v1, v2 with
      | Interv (a1,b1), Interv (a2,b2) ->
         let a = if X.compare a1 a2 <= 0 then a1 else a2 in
         let b = if X.compare b1 b2 <= 0 then b1 else b2 in
         normalize a b
      | _ -> Any

    let subsumed v1 v2 =
      match v1, v2 with
      | Any, _ -> true
      | _, Any -> false
      | Interv (a1,b1), Interv (a2,b2) ->
         X.compare a1 a2 <= 0 && X.compare b1 b2 >= 0
                           
    let bottom = Any

    let parse = dcg
      [ "["; a = X.parse;
        ( ","; b = X.parse; "]" -> normalize a b
        | "]" -> normalize a a )
      | "][" -> Any ]
                   
    let print = ipp
      [ Any -> "]["
      | Interv (a,b) -> when (X.compare a b = 0); "["; X.print of a; "]"
      | Interv (a,b) -> "["; X.print of a; ","; X.print of b; "]" ]
  end

module Set(X : T) : T =
  struct
    type t = X.t list

    let normalize (v : t) : t =
      let rec aux acc = function
        | [] -> acc
        | x::v1 ->
           if List.exists (fun x1 -> X.subsumed x x1) v1
           then aux acc v1 (* x removed *)
           else
             let v1 = List.filter (fun x1 -> not (X.subsumed x1 x)) v1 in
             aux (x::acc) v1
      in
      List.rev (aux [] v)
           
    let subsumed v1 v2 =
      List.for_all
        (fun x1 ->
          List.exists
            (fun x2 ->
              X.subsumed x1 x2)
            v2)
        v1

    let similarity v1 v2 =
      let v =
        List.fold_right
          (fun x1 res ->
            List.fold_right
              (fun x2 res ->
                X.similarity x1 x2 :: res)
              v2 res)
          v1 [] in
      normalize v

    let bottom = [X.bottom]

    let parse = dcg
      [ "{"; lx = LIST1 X.parse SEP ","; "}" -> normalize lx ]

    let print = ipp
      [ v -> "{"; LIST1 X.print SEP "," of v; "}" ]
  end

(* test modules, for type checking *)

module ParamIntervalInt =
  struct
    let normalize (a,b) = if b-a <= 20 then Some (a,b) else None
  end
  
module Test1 = Nominal(String)
module Test2 = Interval(Int)(ParamIntervalInt)
module Test3 = Set(Test2)
